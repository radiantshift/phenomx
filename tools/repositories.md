---
---
## for Phenomx Knowledge base.


### Tools: 

 - [obsidian][ob]: PhenomX' Brain
 - [git][frk]: PhenomX Notary
 - [keybase][kb]: PhenomX Secures Vaults
 
 ## productivity tools 
 - [slack](https://phenomxhealth.slack.com/archives/C030T5D4NE8): communication
 - [confluence](https://phenomx.atlassian.net/wiki/spaces/PhXApp/pages): shared documentation (initially google drive)


![fork](https://git-fork.com/images/logo3.png)
<style>img[alt=fork] { float: right; width: 20vw; }</style>


### Repository:
 - <git@gitlab.com:phenomx/knowledge.git>
   or <https://gitlab.com:phenomx/knowledge.git>
 - <git@gitlab.com:phenomx/algorithm.git>
   or <https://gitlab.com:phenomx/algorithm.git>
 - [keybase://team/phenomx/development.git][kb.dev] (trade secret)

[kb.dev]: keybase://team/phenomx/.kbfs_autogit/development

### Privacy Vault:
 - secret-rooms: keybase://team/phenomx/secret-rooms (investors and board)
 - survey: [keybase://team/phenomx/survey](keybase://team/phenomx/.kbfs_autogit/survey) (user survey data)
 - data: [keybase://team/phenomx/data](keybase://team/phenomx/.kbfs_autogit/data)

### client side tool:

 - installation:
   1. create account on <https://gitlab.com>
      (ask [[@michel]] to be invited to phenomx repositories)
   2. install "[the fork][frk]"
   3. install "[obsidian][ob]"
   4. optionally "[keybase][kb]" (if you plan to manipulate secure data)
- setup
	1. download the following zip file ([algorithm.zip][1]) and extract it at a location of your choice
	2. open the folder within [[obsidian]]

[1]: https://ipfs.safewatch.care/ipfs/QmNcgaR4R75oNX327JrB7riS1mL7ucqhiRtKEyZDoGMuuT/algorithm.zip


[frk]: https://git-fork.com/
[ob]: https://obsidian.md/
[kb]: https://keybase.io
