- xcrun error solution: https://www.ellyloel.com/garden/obsidian-git-xcrun-error/

```
plugin:obsidian-git:30738 Error: xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun```
```
  The reason you are seeing this error is that your system doesn't have **Xcode** installed. Either you have never installed it on your Macbook or it got uninstalled during any OS update.
```bash
xcode-select --install
```

#### about git credential

open a "terminal" again type the command  
`git config --global credential.helper osxkeychain`  
