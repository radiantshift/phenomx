#

# intent: publish images to Pinata


echo "--- # ${0##*/}"
find . -name '*~1' -delete

tic=$(date +%s%N | cut -c-13)
top=$(git rev-parse --show-toplevel)
cd $top/assets

for d in branding logo colors QRCodes source tablets team; do
  echo d: $d
  ( cd $d; mkindex.sh)
  ls -l $d/index.html
done

# get previous hash ...
ipath=$(ipfs name resolve $key --cid-base=base58flickr)
echo ipath: $ipath
qmprev=${ipath#/ipfs/}

echo "<h2>phenomx assets (webp)</h2>" > index-webp.htm
echo "<h3><a name=webp>WEBPs</a></h3>" >> index-webp.htm
ls -1t *.webp | while read i; do
echo "<img src='$i' alt='$i' title='$i'>\n" >> index-webp.htm
done
echo "<h2>phenomx assets (jpeg)</h2>" > index-jpg.htm
echo "<h3><a name=jpeg>JPGs</a></h3>" >> index-jpg.htm
ls -1t *.jpg | while read i; do
echo "<img src='$i' alt='$i' title='$i'>\n" >> index.jpg.lst
done
echo "<h2>phenomx assets (png)</h2>" > index-png.htm
echo "<h3><a name=png>PNGs</a></h3>" >> index-png.htm
ls -1t *.png | while read i; do
echo "<img src='$i' alt='$i' title='$i'>\n" >> index-png.htm
done
echo "<h2>phenomx assets (svg)</h2>" > index-svg.htm
echo "<h3><a name=svg>SVGs</a></h3>" >> index-svg.htm
ls -1t *.svg | while read i; do
echo "<img src='$i' alt='$i' title='$i'>\n" >> index-svg.htm
done

cat index-hdr.htm index-webp.htm index-jpg.htm index-png.htm index-svg.htm |\
  sed -e "s/:qmprev/$qmprev/g" > index.htm.tmpl

qm=$(ipfs add -r . -Q --cid-version=0 --cid-base=base58flickr)
echo qm: $qm

ipfs files stat /ipfs/$qm
echo url: https://ipfs.safewatch.tk/ipfs/$qm
echo url: https://gateway.ipfs.io/ipfs/$qm

# Pin Services API ...
if ipfs swarm addrs local; then
  ipfs pin remote add --service=nft --name=phenomx $qm &
  if ! ipfs pin remote service ls | grep -w phenomx -q; then
    jwt=$(cat $top/keys/pinata_api_keys.jwt_token.yml | grep -w jwt | tail -1 | cut -d' ' -f2)
    ipfs pin remote service add phenomx https://api.pinata.cloud/psa $jwt
  fi
  ipfs pin remote rm --force --service=phenomx --name=qmroot
  ipfs pin remote add --service=phenomx --name=qmroot $qm &
  echo url: https://gateway.pinata.cloud/ipfs/$qm
else
  echo ipfs is offline
fi



# get phenomx key
if ipfs key list | grep -q -w phenomx; then
  key=$(ipfs key list -l --ipns-base=base58flickr | grep -w phenomx | cut -d' ' -f1)
  echo "key: $key"
else
# Z5azMnuicXMuwPGc7EyrZjdXGfxWtwuuB4GgzXYvjucAa3Ri1XsR1Gh: Hannah T. Binetti
  key=$(ipfs key gen phenomx --ipns-base=base58flickr)
fi

if qmtemp=$(ipfs object patch rm-link $qmprev assets); then
  echo qmtemp: $qmtemp
else 
  qmtemp=$qmprev
fi


# qm for website
top=$(git rev-parse --show-toplevel)
qmsite=$(ipfs add -r $top/_site --ignore assets -Q --cid-version=0 --cid-base=base58flickr)


if qmtemp=$(ipfs object patch add-link $qmsite prev $qmprev); then
  qmroot=$qmtemp;
else
  qmroot=$qmsite
fi
if qmtemp=$(ipfs object patch add-link $qmroot assets $qm); then
  qmroot=$qmtemp;
fi
sed -e "s/:qmsite/$qmsite/" index.htm.tmpl > index.htm~
sed -e "s/:qmroot/$qmroot/g" index.htm~ > index.htm
echo qmroot: $qmroot

qmsite=$(ipfs add -r $top/_site --ignore assets -Q --cid-version=0 --cid-base=base58flickr)
if qmtemp=$(ipfs object patch add-link $qmsite prev $qmprev); then
  qmroot=$qmtemp;
else
  qmroot=$qmsite
fi
if qmtemp=$(ipfs object patch add-link $qmroot assets $qm); then
  qmroot=$qmtemp;
fi
sed -e "s/:qmsite/$qmsite/" index.htm.tmpl > index.htm~
sed -e "s/:qmprev/$qmroot/" index.htm~ > index.htm
echo qmroot: $qmroot

ipfs name publish --key=$key $qmroot --allow-offline --ipns-base=base58flickr --cid-base=base58flickr &
qrencode --size=4 -o $top/images/QRassets.png https://ipfs.safewatch.care/ipns/$key/assets/index.htm
qrencode --size=4 -o $top/images/QRsite.png https://ipfs.safewatch.care/ipfs/$qmroot
cp -p $top/images/QRsite.png . # for "chaining"

echo $tic: $qmroot >> $top/_data/qmroot.log
echo "--- # ipfs hash for assets\nqmroot: $qmroot" > $top/_data/ipfs.yml
git add $top/_data/ipfs.yml
git commit -m "assets published under $qmroot on $(date)"

echo "url: https://ipfs.safewatch.care/ipns/$key/assets/index.htm"

exit $?
true;
