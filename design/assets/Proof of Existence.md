prior date: 2022/11/20

date of filing: 2022/11/24

```yaml
--- nftfy.sh
qm: QmPn49g5H5i5TonhBpZWpZ2Ukz96AYbg2p5ah6Jn52PVuK (gitid-prev.png.ots is the same)
Success! Timestamp complete
git64: dBRTKkhyinjDa3a70FZ29PNPMFA=
qm: z8bvizU6eZEvUhNwPJnqvjUDMRMtd6YyZ
url: http://127.0.0.1:8080/ipfs/z8bvizU6eZEvUhNwPJnqvjUDMRMtd6YyZ/gitid.png
qm: QmZKPPG9YaxUXbFEDQWKx2txYGpAqNkhReL21pzjaXFzzL
# NFTname: Brandon's GIT
git64: dBRTKkhyinjDa3a70FZ29PNPMFA=
qm: QmZKPPG9YaxUXbFEDQWKx2txYGpAqNkhReL21pzjaXFzzL
```

proof of assets existence : ![gitid](http://127.0.0.1:8080/ipfs/z8bvizU6eZEvUhNwPJnqvjUDMRMtd6YyZ/gitid.png)