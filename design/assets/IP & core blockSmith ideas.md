
### apparatus
- wearable monitor (photonic based)
- pee smart strip
- temperature reading with BLE neckeless, with heatharvesting self powered
- [biostarks][1] blood finger prick


[1]: https://biostarks.com

###  PhY methods (patentable)
- use of push to talk for symptom tracking
- proxy tracking (not-alone / metoo symptoms captures)
  
### clearSky (license)
- edge AI for ASR (STT)
### blockSmith (licenses)
- small data ML (n to 1)
- sentiant recommendation engine
- (personalized) diet generation methods
- purse (centralized data), reward system
- rekeyable "wallet/purse"
- group identities
- name based identity
- geometric scaling model (1 node per 150 users)
- understandable AI
- bayesian + ML {,...}
- self shared secret (DH)

## App Design

wireframe:
![[2022-09-09 19.03.The Pause App. wireframe]]
