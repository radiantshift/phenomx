 > 7% of men are color blind, compared with only 0.4% of women.
 
 ![[top 20 colors across industries.png]][details](https://www.visualcapitalist.com/wp-content/uploads/2017/02/color-by-industry-infographic.jpg)
 
- branding for women : 
  - https://emilypost.com/advice/color-and-your-professional-brand-women
  - https://brandingforwomen.com/
  - https://www.corporate-eye.com/main/how-gender-affects-color-marketing-and-branding/
  - https://jessicagingrich.com/color-psychology-and-branding/
  - https://kdesign.co/blog/feminine-color-palette-ideas/
- colors for CTA : see https://convertkit.com/call-to-action-colors
  
  ![[Gender-n-Colors.png]]

read more: https://blog.vmgstudios.com/brand-color-psychology-men-vs.-women



Note: skin color:
- hex: #E8BEAC
- RGB Values (232, 190, 172)
- CMYK Values (0%, 18%, 26%, 9%)
- HSV/HSB Values (18°, 26%, 91%)
- Closest Web Safe #FFCC99
  
there is 4 types of skin hue:
  1.  White Skin Color Code : #fde7d6 
  2.  Light / fair
  3.  Medium
  4.  Deep (Dark)