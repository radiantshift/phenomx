

### Git repository:
- old website assets: https://gitlab.com/xn--iy8h/websites/phenomx.ch/-/tree/a7ca9b5fd64920233f801208d06b8de07c4e09c5/assets
- new assets repository (courtesy Doctor IT design) <https://github.com/DrI-T/design>
- graphic charter:
  1. [brand colors](https://github.com/DrI-T/design/blob/master/logo/phx-branding.svg)
  2. trademark, textmark, drawingmark : [![branding](https://gitlab.com/xn--iy8h/websites/phenomx.ch/-/raw/a7ca9b5fd64920233f801208d06b8de07c4e09c5/assets/logo/phx-branding.svg)](https://gitlab.com/xn--iy8h/websites/phenomx.ch/-/blob/a7ca9b5fd64920233f801208d06b8de07c4e09c5/assets/logo/phx-branding.svg)
![[phx-gold-logo.svg]]
![[cgm-sticker.svg]]
![[05 design/Brand/experiments/phx-gold-w-solid-fly-period.svg]]
![[little-golden-bfly.svg]]
![[images/little-golden-bfly-solid.svg]]
![[butterfly-green.svg]]