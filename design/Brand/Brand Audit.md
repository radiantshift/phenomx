## brand audit (Ari)

This file is store on gitlab @ : https://gitlab.com/phenomx/knowledge

1. audience split and adjusted "voice/tone" : pre-menopause | post-menopause | post-post menopause
2. branding : umbrella + application (phenomX + pause) or one unique brandname
3. 2 sites  
	1. e-shop
	2. pause
4. colors : reading the description ... more emerald and gold than blues !
    palettes: [examples](https://digitalsynopsis.com/design/color-palettes-combinations-schemes/)
5. need consistency around imagery
6. clearer message:
	1. How can we help you ?
	2. this is one way ... phenotype assessment etc.
7. Our approach
	1. process page the "5Ps" (maybe pointing to a blog article)
	2. personalization beyond marketing quiz: our science; approach to precision nutrition.
8. more social proof
	1. certifications
	2. press mentions
	3. partnerships / bodies
9. about / podcast
	1. why it matters,
	2. butterfly story: transformation, meaning

pricing for Ari's agency : $1350/10h/month for min. 3 months (options for 10h,20h,40h)