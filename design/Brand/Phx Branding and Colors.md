## Color palette

![[phx-branding.svg]]

Hex values:
```yaml
Tyrian purple: #300030
Minsk: #443774
Calypso: #3d677f
Whisper: #efeaea
Spanish White: #ddccb8
Goldenrod: #af883a

```

- colors names : <https://www.benjaminmoore.com/>
- hue to name : https://www.color-blindness.com/color-name-hue/
- hex to name: https://www.colorhexa.com
- find colors names: https://icolorpalette.com/color?q=
- Coolors palette : https://coolors.co/300030-443774-3d677f-efeaea-ddccb8-af883a
- Canva's palette:  https://www.canva.com/colors/color-palettes/
- Colors Scheme: https://www.schemecolor.com/?s=action
- Color Psy: https://www.supermoney.com/colors/
  
  ## background ?

- accent : gold
- primary: purple tone ([spanish violet](https://www.schemecolor.com/sample?getcolor=46296e))
- secondary: cyan / emerald / green tone


![[one-of-them.png]]