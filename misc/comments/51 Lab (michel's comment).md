url: <https://matterhealth.submittable.com/submit/236922/51-labs-2022?step=submission>
for changes and "highlight" please check the git repository:
 <https://gitlab.com/phenomx/knowledge/-/commits/master>


- criteria: empower and support menopause women+
  ### needs: of underrepresented women
- priority areas:
- _equitable access to care_
   tracking of symptoms are done totally anonymously we don't ask any private data to our user
   consequence totally equitable accross ethnicities and gender+
   (the application doesn't use the concept of user account)

- _creating awareness_
  - community oriented app w/ educational material, AMA etc. participatory educational game etc.
  - pair support w/i app

- _Addressing and tracking symptoms_
   "empathethic symptom and lifestyle tracking" : "me too" button
   
   [^1]: inclusive: women+
   [^2]: platform with smartphone  sounds very wierd
   [^3]: provides as space for 
   
  ### submission:

**Short description of company*******

  

PhenomX Health is a precision nutrition for women, FEMTECH company. We empower women+ through personalized nutrition and wellness to take control of their menopause and healthy aging journey. The PhenomX woman benefits from the latest health science and molecular nutrition systems powered by deep tech. The PhenomX app enables personalized nutrition to support the female menopause journey, through its user friendly, simple interface.

  

With our focus on personalization we want to create a more inclusive world and empower women of any origin to optimize and embrace their wellness after the period.

  

**Describe the problem you are solving and the connection to one or a combination of the challenge tracks.*******

  

Problem: Women on their hormonal health and aging wellness journeys have been ignored!

  

With over 1 billion women in menopause in 2030, we know 70% will be affected with symptoms severe enough they will impact their work and family life balance. 75% women that go to the doctor with menopause symptoms will return home with no solutions. This is because currently our society invests only 4% in R&D for women's health and only a tiny percent (approx .002) in aging women's health. We also know menopause starts earlier in black women who tend to experience more severe symptoms; and these women are under-represented in funded research studies. Our team has experienced this suppression of R&D first hand whilst conducting cutting-edge women’s nutrition and hormonal health research. We have been on the medical front-lines with female patients who needed more support and we have been in female dominated professions whose impact on women’s health was oppressed due to these societal factors. Our society has created an $850B healthcare burden and PhenomX wants to change this!

  

Current menstrual cycle apps focus on tracking the cycle symptoms and fertility which no longer pertain to the menopause journey. Digital platforms that offer diet and lifestyle advice for middle-aged women use a one size fits all approach. How does she know which bioactive foods, nutraceutical supplements and diet strategies are best for her and how does she monitor what is working? With a busy lifestyle, running a full-time career and household, women want to be able to optimize their health in a simple, efficient and sustainable way, whilst feeling supported and trusting the advice that they receive.

  

**Describe your solution, how it addresses the unmet need and how it enhances value to customers and stakeholders**.*****

  

Both our digital platform and smartphone applications offer a user friendly interface for any age and level of technological ability. The first priority is creating awareness about menopause and midlife to “Unpause the Talk” (https://www.phenomxhealth.com/forum). Our users track their symptoms anonymously. No private data is requested. The consequence is equitable across ethnicities and women+. We are also developing an empathetic symptom tracker with advanced technology around which we are very excited (stay tuned).

  

Symptoms, diet, micronutrient, carbohydrate metabolism, hormone scores and more are calculated regularly for the user. Our customer can select the options they find most interesting. Users receive recommendations for realistic, achievable diet changes, special herbs, teas, optimal nutrition supplement ingredients and the best local supplement brands to purchase. Her non-invasive assessments (such as micronutrients, genetics, wearable device metrics) are incorporated into her application dashboard. Expert nutrition and health chat bots are available at all hours of the day, at her fingertips! Our expert augmented AI recommendations get better with age, just like her!

  

Membership of the app and digital platform, affords our customer the opportunity to participate in new personalized assessment programs, our nutrition longevity and rejuvenation diet program, and holistic nutrition coaching from registered dietitian nutritionists trained in integrative and functional medicine. Customer loyalty and app usage is rewarded through a system of virtual vitamin NFTs (non-fungible tokens on the blockchain), which translate to discounts and product rewards. She joins our digital community of like-minded women. She can purchase recommended products directly from our digital platform. Her experience is real simple, real easy, private and efficient enough to fit into her daily life.

  

Our international smartphone app and web-based platforms are built for US, UK and CH/EU. Since Black, Asian women have earlier menopause and more severe symptoms, we are keen on addressing their specific cultural needs related their health and nutrition. According to the SWAN study, these population groups prefer complementary alternative nutrition (e,g, supplements) to treat their menopause symptoms verses white women. Our digital platform is tailored for them!

  

Mockup video:

https://www.youtube.com/watch?v=FYANhargjaM

  

**List and describe any customers, partnerships or collaborations in which you have previously been engaged or that are currently ongoing.**

  

Creating a micronutrient dried blood spot test for menopausal women with Biostarks (Swiss and US labs). https://www.phenomxhealth.com/product-page/optimal-nutrition-test. The Biostarks Test is already sold on our website as a standard micronutrient test. Creating easy spoken symptom and nutrition intake reporting in collaboration with ClearSky Edge AI transcription (start-up). https://www.linkedin.com/company/clearsky-ai/?originalSubdomain=ch. Selling Bossa Bars as an early affiliate relationship. https://www.phenomxhealth.com/usproducts. Insentials is a personalized nutrition for women company but without a focus on menopause/middle age with whom we are pursuing a collaboration in 2023. Partnerships and collaborations are very important to us since we are creating a personalized nutrition ecosystem to maximize our success in accordance with the recommendations in the following whitepaper: https://nx-food.com/personalized-nutrition-whitepaper. We participated in the first FEMTECH accelerator in Switzerland, Tech4Eva, in 2021 and continue to be active as local alumni. We are currently a member of the La Forge accelerator that provides a space for our offices on the EPFL campus and promotes us as part of the Swiss start up community. Finally, we are network partners of the FoodValley Netherlands and we are active in the Personalized Nutrition community, collaborating with a handful of personalized nutrition start-ups to design a pilot study on menopausal women in the workplace. We are working towards a partnership with an appointment scheduling digital health company in Europe as a pilot for the potential to set up this same kind of relationship in the US to promote our platform. We are eager to build our partner community in the US where we think we will have the most traction in the marketplace early on.

  

**Describe how your solution fits within the competitive landscape and aspects of your competitive advantage.*******

  

Competition

Market desirability and timing of the approach is supported by the fact that all of our competitors are new companies within the last 1-4 years. Presently, we are a first mover in precision nutrition for menopause and aging. Midday (formerly Lisa Health) is our newest competitor offering an educational and symptoms tracking app in collaboration with the Mayo Clinic. Hologram sciences launched Phenology, a menopause supplement company with a focus in personalized nutrition. They have received €100M in funding from DSM, a Dutch supplement ingredient company. Their focus is on selling products which utilise DSM ingredients, leveraging personalization as their sales tool. Other businesses, such as the Join Olivia app, have a nice look and feel and ideas embedded in their app for symptom tracking. They lack a dynamic and effective user experience and are likely struggling with continuous user traction.. Other apps support more of the medical marketplace, such as Balance, or the need for awareness, Lisa Health, or simply offer tracking and sell menopause products, mySisters.

  

Reasons to Believe in PhenomX: point of Differentiation

1. Nutrition and life science expertise and knowledge to truly personalize nutrition.

Many companies claim to "personalize" to sell their nutrition supplement SKUs that contain at least one patented ingredient that may or may not affect the biological pathway of an individual. Thus, their approach to personalization is simply a selling tool for their 3-5 products for menopause that might range from vaginal dryness to a nutritional for menopause (i.e., Kindra). At PhX, we have 25+ years of true personalization experience, designing gene and other -omics tests, as well as, nutrition and diet advice that honors the individual's unique biochemistry. Thus we make use of the thousands of fantastic product options already in the marketplace and direct our users to the best options for them. We are the trusted source of personalized (precision) nutrition recommendations for women’s health.

  

2. Blockchain and digital development expertise that takes care of our planet and protects humanity.

Our server-less, decentralized blockchain approach sets us apart. Referred to as our PhenomX Data leger, we protect security (no-one can read the data), and privacy of data (no-one knows whose it is) and to which no-one has custody (knows where it is). Our nutrition expertise is augmented by artificial intelligence and our machine learns from user experience. Our system is scalable from the start and privacy protected (GDPR, HIPAA compliant). We use non-fungible tokenization to encourage loyalty and community participation.

  

3. We have established relations with partners providing additional revenue streams and a strong network of knowledge and product sourcing. With our 25+ years of experience in a variety of disciplines, we have created a strong, world-wide network of experts and technology collaboration partners to build the ultimate precision nutrition ecosystem for her.

  

4. We have deep consumer understanding and value. Our team’s clinical practice/patient care experience is coupled with scientific research and claims translation which supports our strong "science translated to action" approach, which in turn underpins our proprietary algorithm development.

  

5. We are committed to addressing racial ethnic disparities and women + needs in the US. Of note, an interesting article here: https://www.frontiersin.org/articles/10.3389/fdgth.2022.807886/full.

  

We are a differentiated platform for personalized nutrition and next generation holistic health because we are creating a new approach to women's health, changing the way hormonal health and nutrition are viewed while maintaining international, ethnic and culturally diverse accessibility.

  

**Describe your business model, namely the solution-market fit, and how your solution delivers value to customers and stakeholders.**

  

Our first priority is our direct to consumer approach (app subscription) which allows us to build data and insights, relevant for convincing other customers to work with us. Funding also comes from the affiliate marketing payments from our physical product partners and test companies.

  

Second priority is our clinician approach, which is particularly important for the USA market and leverages our existing functional medicine ecosystem and relationships. These relationships have a ‘multiplier effect’ as clinicians have portfolios of clients and thus onboarding one clinician, represents multiple sales. Additionally, these clinicians have a strong understanding of the diverse marketplace of women we wish to serve.

  

Our longer term, third priority, is to offer a personalized nutrition engine approach for enterprises, in a SaaS format, for personalized product recommendations on limited portfolios.

  

By creating focus on a neglected population with unmet needs that are solvable with a personalized, precision approach to nutrition; and using the latest computational and block chain methods to handle dynamic data whilst protecting privacy in an energy efficient approach; PhenomX Health is creating a successful model that could be replicated for other populations.

  

**Describe your team (who, accomplishments, advisors, etc.).*******

  

We are an international team of doctorate level scientists, clinicians, engineers, business strategists and operational experts. We come from multinational food & consumer health companies, start-ups, blockchain, insurance, women's health research & FemTech innovation. Our diverse & complementary skills & experience is combined with a strong passion to empower women & enable them to live better, healthier lives; and strengthen their contributions to society.

  

See below high level recap of current team:

  

Executive Team : CEO, CTO, COO (3 FTEs)

Core Team: Head of Nutrition, Partnerships, Tech Dev (1 FTE*)

Advisors : Digital Health, Nutrition, Business (6)

  

*Plan to hire all 3 as of January/February + marketing specialist

  

See below more details about team members.

  

Dr. Colleen Fogarty Draper PhD, RD is the Co-Founder and CEO of PhenomX Health. Dr. Draper is an entrepreneur and clinical dietitian with a PhD in nutrition systems biology and metabolic health with an emphasis in women’s health and gender dimorphism. She has over 25 years experience in personalized nutrition including 9 years working in large food multinationals, start-ups, academic universities and teaching hospitals. In her quest to be a good mother and emulate the self-care she would like to see in her daughters, Dr. Draper was amazed at the limited options available to manage their hormonal health naturally, consistently and with limited side effects. Realizing she faced these same limitations in her clinical practice years ago and reflecting on her own clinical and scientific research experiences in women's health, she decided it was time to make a difference. As a perimenopausal mother of 3 teenage girls, Colleen Fogarty Draper MS, RD, PhD founded and created PhenomX Health; a femtech pioneer in perimenopausal health and nutrition. She also created a world-class nutrition science translation consultancy, NutraUHealth. Dr. Draper is very passionate about connecting new technologies to improve human health, as well as, the health of our planet. Click here for Dr. Draper's story.

  

Dr. Michel Combes PhD is Co-Founder and CTO of PhenomX Health, is a computer engineer and software designer with startup and broad corporate experience. He is passionate about creating technology that optimizes humanity, including sustainable blockchain technology. He brings his immense start-up and tech dev experience to PhenomX Health and is the driving force behind our creative technology development and the first to place women's health on the blockchain.

  

  

Jerome Michaud is PhenomX Health’s Chief Operations Officer. He brings his strong experience in the FMCG (fast moving consumer goods) and OTC (over the counter) industries (10+ years at P&G), successful entrepreneurial experience (exit in 2016) and international network to the benefits of the team. His areas of expertise: business and supply chain operations, consumer product development, team management/development, partnerships with corporates.

  

  

Dr. Ksenia Tugay, PhD, MBA is our FemTech Innovation advisor at PhenomX Health. She is a life scientist and business woman with extensive experience in health innovation. Dr. Tugay is managing the PhenomX Health investment and accelerator program strategy as well as scientific translation communications and leading our legal operations and business structure development.

  

  

Elena Donderfer, MBA brings her wealth of expertise in strategy & business development, business transformation, retail & consumer goods. With over 12 years of business experience, Elena supports our business strategy and consumer strategy development including our consumer market research and pitch deck communications.

  

  

Alicia Trocker, MS, RDN, IFNCP is the Clinical and Functional Nutrition Lead at PhenomX Health. Alicia is a clinical dietitian with over 25 years experience in holistic approaches to chronic illness, transplant nutrition and lifestyle & wellness approaches to health. She is passionate about integrative approaches to health and helping clients find optimal nutrition for their personal needs. Alicia is leading the development of our diet programs and coaching services.

Kathie Madonna Swift, MS, RDN, LDN, FAND, EBQ is a registered dietitian nutritionist who has been dubbed “the mother of integrative and functional nutrition” by thought leaders in the field. Kathie has authored "The Swift Diet" and is a contributing author for numerous books and medical textbooks, serves on numerous advisory boards, has created multiple organizations to support integrative and functional medicine education and training. She is a brilliant creative clinician, advisor, and thought-leader who has pioneered leading-edge functional and integrative medical nutrition programs. She is our first Key Opinion Leader & Advisor for PhenomX Health to ensure we are providing the best guidance to our users, members and partners.

  

Reto Hartmann is the PhenomX business coach with a background in the pharma, biotech and medtech industries. He has over 17 years of management and sales experiences in the life sciences. Reto is a former VP of Sales and Marketing for a small biotech company as well as being a top Account Manager for a multinational B2B life science company and is an accredited member of the Innosuisse and Platinn coaching networks. Reto is also a molecular biologist, having completed his undergrad work at the University of Massachusetts and Graduate work at the Ludwig Institute for Cancer Research in Lausanne.

  

The following individuals have joined our team this month, October 2022. as we prepare for our first round of financing.

  

Farah Al-Hawasli MPH, RDN,LD

Head, Precision Nutrition

  

Vera Steullet, PhD

Digital Health Advisor

  

Vanessa Kendrick

Head, Strategic Partnerships

  

Alysha Reinard, PhD

Tech Dev

  

Heiko Schickel

Independent Board Member
