- Did you know that Calmness is a new superpower?  
- The moment you lose your calm, you will lose & the opponent wins!    
----
1. walk: walking helps clear your mind.
   it offers you a different perspective.
2. Indulge: take a day off to spend a whole day doing exactly wat you want.
----
3. Be Generous: Give something to a total stranger.
   Acts of giving makes us feel warm and fuzzy inside.
4. Sit in a coffe shop or a busy street and soak up your surroundings. You don't have to talk to people
----
5. Educate yourself: research waht it is you are experiencing.
   Arm yourself with knowledge and the resources to tackle the problems head-on.
6. Preparation: Write the day's to-do list the evening before.
----
7. Strengths: Write down a list of 20 of your strengths.
8. Keep going forward: Keep taking small steps, no matter what. Being stagnant doesn't serve you.
----
9. Re-visit an old hobby: if you don't have one, create one.
10. Prioritize: Decide what's important right now. Say no to extra obligations.
----
11. Sleep: get enough rest.
    Sleep 7-9 hours each night.
12. Be silly: Do something that you did as a child. Don't take life too seriously.
____
13. Cry: Realease all that emotion. You will fell better.
14. Check your self-talk. Negative self-talk doesn serve you.
----
15. Journal: Develop a habit of journaling. This will help free your mind.
16. Remind yourself that life is a journey. Remember that what you are going through is temporary, It will pass.
----







Apply these hacks your everyday if you think they are useful