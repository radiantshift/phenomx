

Article 1: Ownership

1.1 The company is owned by three co-founders: [Name of CEO](https://chat.openai.com/%5Bpercentage%5D%25), [Name of CTO](https://chat.openai.com/%5Bpercentage%5D%25), and [Name of COO](https://chat.openai.com/%5Bpercentage%5D%25).

1.2 The ownership percentage of each co-founder is as follows:

1.3 The total ownership percentage of all co-founders is 100%.

Article 2: Voting Rights

2.1 Each co-founder has the right to vote on matters affecting the company, including the appointment of directors, the adoption of bylaws, and the sale or merger of the company.

2.2 A decision on such matters requires the approval of at least [percentage]% of the co-founders.

Article 3: Dividends

3.1 The company may distribute dividends to the co-founders from time to time at the discretion of the board of directors.

3.2 Dividends will be distributed in proportion to each co-founder's ownership percentage.

Article 4: Transfer of Ownership

4.1 A co-founder may transfer their ownership interest in the company to another party with the written consent of the other co-founders.

4.2 The transfer of ownership will be subject to the following conditions:

-   The transferee must be approved by the board of directors.
-   The transfer must be for fair market value as determined by an independent valuation expert.
-   The transfer must be in compliance with all applicable laws and regulations.

Article 5: Dispute Resolution

5.1 In the event of a dispute among the co-founders, the parties will attempt to resolve the dispute through mediation or arbitration.

5.2 If the dispute cannot be resolved through mediation or arbitration, it will be resolved in accordance with the laws of the state of [state].

This is just one example of a share agreement and the specific terms will vary depending on the needs of your company. It's important to consult with a legal professional to ensure that your share agreement is tailored to your company's needs and complies with relevant laws.