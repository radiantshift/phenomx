### Equity ought to be equitable ...


Principles:
- founders bring their stories ...
- the merge of all stories make the company

* equity is to reward pre-seed effort and risk investments that enable the entreprise.
	* shares grows even if you do nothing
* options are for contributors we want to keep.
	* options you have to get them (at discount) and work for its values


Models: 

>Sure, I'd be happy to help you draft a share agreement for your company. A share agreement is a legal document that outlines the terms and conditions under which the co-founders of a company will hold and transfer ownership interests in the company. It is important to have a clear and detailed share agreement in place to ensure that all co-founders are aware of their rights and responsibilities and to minimize the risk of disputes or misunderstandings in the future.

Here are some key points that you should consider including in your share agreement:

1.  Ownership percentage: The share agreement should specify the percentage of ownership that each co-founder holds in the company. This can be based on the contribution of each co-founder to the company (e.g. capital investment, time, resources, expertise) or on some other agreed-upon basis.
    
2.  Voting rights: The share agreement should outline the voting rights of each co-founder, including the percentage of votes required to make decisions on matters such as the appointment of directors, the adoption of bylaws, and the sale or merger of the company.
    
3.  Dividends: The share agreement should specify how dividends (if any) will be distributed to the co-founders and whether they will be distributed in proportion to each co-founder's ownership percentage or on some other basis.
    
4.  Transfer of ownership: The share agreement should outline the conditions under which a co-founder may transfer their ownership interest in the company, including any restrictions on the transfer of ownership (e.g. the requirement to obtain the consent of the other co-founders) and the process for valuing the transferred ownership interest.
    
5.  Dispute resolution: The share agreement should include provisions for resolving disputes that may arise among the co-founders, such as mediation or arbitration.

>Certainly! Here is an example of a share agreement for a company with three co-founders: [[share-agreement]]

It's important to note that the specific terms of a share agreement will depend on the specific needs and circumstances of your company. It's advisable to consult with a legal professional to ensure that your share agreement is tailored to your company's needs and complies with relevant laws.


options:
- EGD Co-Founder split
- PieSlicer
- cost of replace