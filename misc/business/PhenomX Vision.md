We are currently collecting the team inputs about PhenomX culture and values ...
please add your comment and feedback about our vision/mission statement



## PhenomX Health
   With us You'd want to start your menopause sooner 
---
Our values & [pledge](obsidian://open?vault=EcoOrg&file=00%20notes%2FThe%20commitment%20to%20our%20values)


### The Vision: ...

- phenomenal human in harmony with their ecosystem
- biodiverse intelligent organi-verse in recovered equilibrium
* Every life form will reach its heath potential with technology similar to phenomx one
* we connect people to their higher purpose
![[PhenomX Vision 2022-09-29 11.54.06.excalidraw 1]]
### Our Mission:


- restore the ecosystem health starting with menopause women
- make it first, implement changes, try it out
- closing the diversity gap 
- protect / regenerate life

---

### Our values:
 - Seed intention
 - Syntropic behavior
 - Biodiversity
 - Empirical science
 - Non Violence (non invasive)
 - Nature Rhythm
 - Organic Life

---

The technology : [[PhX Deep Tech]]

The Stack: [[PhXstack]]

---

### Quotes:
- Menopause doesn't make you old
- You don't have to wait for Menopause to be wise
- We are Real, no BS : pure authenticity
- PhX, I should have done that sooner...
- We translate Science into actionable recommendations
- AI get better with age, just like you

