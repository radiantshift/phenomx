## PhenomX health

![[phx-gold-logo.svg]]
courtesy Dr. Michel Combes

---
### Your map to health
  ![[images/PNGs/health-disease-continuum-w-padding.png]]

---
### We have a problem
 * suffering is insane
 * 160M women
 * no generic solution
 ![[images/TAM-SAM-SOM_market.svg]]

---
## our product = health explorer
* your data is your health compass ...
* we give you the map
* you will get there
---
## The app :

![[4-screens.png]]

---
### We scale the unscalable 
* personalization doesn't scale
* we scale personalized health
* supplements != recom / knowledge

---
### Our approach to personalization
* deep health : women health on the blockchain
* no need for big data
* have experts and we "scale" them
* knowledge graph
* tokenized knowledge economy : NFTs 
---
## Translation: a scarce skill
-  We are an established Nutrition authority
- A map for translating science into actions
  > recommendation:
  > zinc with CoQ10 and special diet for 3 days
- Augmented Nutritionist (imitate the Nutritionist not our members / data)
---
## Village approach, the wise women
- scaling expertise / knowledge
- personalization != 1:1 but many:1
- grass root education, raising awareness with "empathetic tracker"
- not alone, build-in community

---
### The map to your destination
* Your Hormonal and health GPS : start point
*  Symptom free : destination
* Waypoints and POIs : NFT on blockchain
* Carbon Negative and High Impact (ESG)

---
### Jen's  health journey
* tokenomics
* gamified e-purse / e-vitamins 
- empowering
- metaverse supplements shop

---

## Amplification 

deep tech : More's Law + Network Effect
* our tech: not against the wall (understandable AI)
* our network: 1M+ users = 104/.16^5
* 5 staged: 104, 655 , 4096,  25600, 160K (MVP1) then 1M (MVP2)
*  each \$1 invested -> $n$ users -> $n^2$ users


---
Thank You

![[phx-michel-bcard3.png]]