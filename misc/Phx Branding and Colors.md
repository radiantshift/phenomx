## Color palette

![[phx-branding 1.svg]]

Hex values:
```yaml
Tyrian purple: #300030
Minsk: #443774
Calypso: #3d677f
Whisper: #efeaea
Spanish White: #ddccb8
Goldenrod: #af883a

```

- colors names : <https://www.benjaminmoore.com/>
- hue to name : https://www.color-blindness.com/color-name-hue/
- hex to name: https://www.colorhexa.com
- find colors names: https://icolorpalette.com/color?q=
- Coolors palette : https://coolors.co/300030-443774-3d677f-efeaea-ddccb8-af883a
- 