
## dunning-kruger syndrome (cognitive bias)

From: Koh Chin Siong:
> I'm a programmer who coded in at least 4 different languages and I'm also a marketer these days.
>
Having gone from back end to front end, I can only say this:
>
Don't look down on what you don't understand.
>![[cognbias.jpg]]
I did that and I suffered for it. I thought sales people were ripoffs and creeps until I had to do sales.
>
I thought marketing was for subpar products until I had to do marketing.
>
I thought graphics design and good packaging were ‘fluff’.
>
I thought programming was hard until I had to spend my time convincing people I never met online to come pay good money for my services and products.
>
And I thought day to day operations was dead simple even a doorknob could do it.
>
But I didn't expect it to be dead boring and need so much consistent commitment and hard work.
>
And I severely overestimated my capacity for withstanding boring monotonous tasks or hard work.
>
My point is, don't judge easily, until you know what it takes. Classic dunning-kruger syndrome.
>
Does this mean don't drop the guy if he doesn't perform? No. Business needs to move on regardless.
>
But figure out why he's not performing first. Cos there's no guarantee the replacement won't run into the same problems.