---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Perimenopause - 
Menopause Journey ^ubc5MfwN

Healthy Aging ^754ueJ8z

Personalized Nutrition
 ^uj49wDnd

Wellness ^npTpF0Rn

Fitness ^gyisIaAo

40-65 yrs ^e7q2928c

34+ Symptoms Assessment ^YUdKkMXP

Health Diagnostics ^PhyoZf3p

Subscription +
a la cart ^9JWw08S2

Freemium ^pQ0WDP1j

Diet Self Assessment ^gxDzjJQM

Freemium ^aFYe8aPU

Coaching ^VvV9r9AQ

Nutrition recommendations Expert algo ^6QcjULEk

Subscription ^EGwUCuko

Food & Nutrition Products ^GVZGUKhw

Nutrition Supplements ^cqBfeRoF

Digital
Platform ^RfDLrSuP

Blockchain Technology + Machine Learning + Scientific Translation & Integration Algorithm ^7IpzPtqa

Women's Health Clinical & Product Research + New Scientific + Sensor Test Dev   ^0jeQ2VXq

Micronutrient Status ^XCKkWt59

BioStarks - EU, UK?, US ^JvGpKw7q

Continuous monitoring - CGM, Wearables, Biosensing ^dCXM4bhL

DNA + RNA Expression ^BRAANLk7

Microbiome ^QESbFLMp

Metabolomics ^IeiRvKDz

Fitness & Sports Nutrition ^sT0LmoVR

Menopause Stage ^tQRYcgPW

Abbott, Xsensio, Quantum-Op ^CyWY8sFv

Lykon?, PhX own, CaligeniX + Geknowme ^8pzM6ghi

Subscription ^L87BqyQX

Subscription +
a la cart ^uOYdlHKo

E-commerce ^CkwjBIXr

Women's Health 
Integrative 
Nutrition Dietitian 
Network ^7etdlyPL

Mina Health? ^3sGvVLRi

REM Analytics ^V7JNaDfs

Expert approved ^iVIITloz

E-commerce ^kAO9F1KP

FullScript - US, CA ^vS0L1SJh

Urine pH, Hydration, etc. ^gyJeA7rJ

Olive Diagnostics, Urine pH strips ^p3wIxZgM

Bossa Bar - US ^5aYTUXzr

MVP1 Buildout ^iQC1BbjK

PhenomX Health Vision Board ^Sq2HDFy5

Vivi Rocks ^RGa2Fp0a

Insentials, BE, EU ^a3sd5ESJ

IFNA, US ^egEL9a9u


# Embedded files
7b03edbe2dde06b11f4c4640e95070b8084b28d8: [[PhX square Logo 20221022093412_111.png]]

%%
# Drawing
```compressed-json
N4KAkARALgngDgUwgLgAQQQDwMYEMA2AlgCYBOuA7hADTgQBuCpAzoQPYB2KqATL

ZMzYBXUtiRoIACyhQ4zZAHoFWPETKUAdNjYBbGnQT4EOhByjNuAbXBhQYe9Hjj0h

onKT8HjFuy5oeAGwBnpDerJwAcpxi3DwALACMgQDMcUEhEITMACKGCFAIxNwAZgT

MCBkk3BAArPQA1gC0yQCCxDwAUi0IAMoA4gBKACoA0gCa2MwAknD69pDFhPj4PbB

G1ZK42JIiHjYCUKRs9QgA6iRQktwJGcyHx71rzhCCanMOR0IA5pIcCMwWNA3fYQN

hwLaEWDXAAM0IyuA4X3WaDhIMw3EaAA5oRoAskajwaskAJzQgKEjIwDEJEkaQLEz

HJAJxQkAdkxSRqt3uJwAwmx8GxSNUAMSw8XQ96QABGW3qX0+HGI/MFwokhwRzHBp

FMUClEAoFyuaDi0OSGhqNSCpvJNQlwRBkgQhB+erQyUxAQ0rPJxJJcSJSTixLitw

QhWuAZJDOSgQyCuEcCmxEBqEsAF1uUcTj1NqQ4H8AdVmHnZhlpcIlQBRIwmMyp6z

zWz7BxVCQAWWJuAAGgAtAAKABlWdkRggIlXmDwAKpwVIjNhShywRDVXCkI5UFsAX

xCzabkDb6D6AH1pdL2wA1HSYUjFIkRXC8y9VvAdTFLyAr57rzcQHd7nYTaZEUEhD

H0MBQMkUynAWzD0FWABCABWCAdHAPTdoOcSfo4q4SL+bBbk2u4tkB8wgdU9QdDA0

JQGM3akMhzAwMU7YAPLIcQYyXgg7FXHuX5OGuG5Ef+JGAS2h6gegpBxLyiEABInp

emAICMcQcKccSDry0oJB0d64d+Il/gBZFSZREgAGIJEIg46FM2TIZicSnGMxSNDo

uCEB0yGDh0xnCQRonEfMpFNuRrYyRAyTJNgCQRLyFBVjUIzEIZCTEJgMBDC0py8k

F+HoIRYX2Nu+yZiCQhwMQuAFDJCTWmkCTEjw8TshkRAcPU3AcEIyxdWw2AnDJpT4

OUNgRfuy7BegBSYHqGRhL43ABAkqIUStkTRM47XBnEiSYnwIJZLkRgNSUZQVKdMV

CNK2A1O2xQUBE+qLMsqwwMi6CbNsuz6nc2ZnEa1xZg8X0/S8ApVBknw/IWqbAhRY

IQlCQKwvCiI/ZtDjomgjTEriBLBrGG2siSPAhpSGKuRoCRxB60IbZirltck4N8gK

QqihKsL6rKI0JkISoqjz6rkBwWrrrq+qGsQlyxHiGROi60jcKyyMOOUEZAjUrX4q

SyS45AwtJimVhVRRQMQ6WiPFqWAuVsQNbGLqDYWQeVnoHFCVJSlaUZaQWU5XlBVF

T+oXieFlUZDVdWXUCzXrW1HWYl1hA9X1A34ENI266g43lBkxScFAPSEAAXnt6cgq

XZjWbgOhLFSQIZAtbroP2TCEHWqNCOUqCNKgAA6HDtqY/eDx0wikH8VJw5QQxYJ3

EDd6QveT+CA8IEPo/j1vuA76gM8iPPAu4OU3XOOt7cry0RBfH46BiGYTD6t4UCEK

oD8us/0Bgn1NoMwPk/ikGTNnQaIIhQukzgQZei1qjr03hwKeu9h5jwnqg7e09Z7n

ymp4cieFnjXw/kwcIz8fTLXIb4KIHAYhoACGaHgmI7SpEqDkPIidC7XUqDFQ4AxC

BJXYh0XsHBWQ8AGOxOeyQxg9EQgJOuSwVhPGLDDIonMQYK2NKgLWBxgaQ2eK8WGI

J4a/H+KmE2oJwTYEhK3XRmMQQIiRM4Kx+MHFE1Zp6FmJJXLJFZFyEE9iEiYg0CGH

g0I2qYmJJaaE7IkiaLFmqdAYo+YXyFoqZU3NknQEltLHUZg5agxRHSFhNQfRBGZl

aCkjpnSuliKyDQkTYxpADMSYkTC/S12tuGRq5TymBEOidCiZtkwNittrHkvQ7YWI

duuMsIIKwixdrWd2ls461XqgXJqzIWqpziJ1EE3VepoH6lAiigp85jV4SCO464oC

IUzsQTOXxIG5xBKYYgjylQvLeV1S+UB+Q6GbjIQo/Y2CZ07mc951soD3JaKFJ0uA

ZLQoyJ8hFm4kUopzhkOAEL6xWBbI2JsJt7DQhbBM+wxL5g4naqwipTCEjVMCQeHg

jTmnxGZDUdpnSPQUsqgQmwRCTISFIdQnwnBrghPFRQuhDDdFUyZM09hp1OEXQLkX

G6FEjwYGwDAZCl5WQUAAI6NBPGME8J56EUF5MUJqYx3rKMMWot4mjzjaLBrcqZzq

JDGI0aY4QCNZkogyKjWx6MHFWOcTjDI7j2kaDaQcwIzKab+C9C0mobNeUBMSdk3m

aTyxymFqLPNEtNTallhkeWisgREzJESAInoCSpH8Syhwqt6klOhKafWESs3NJzbc

3pa1OUHIpqGEEoyLZoAzJo3M8z7a+sduWZ2rs6zmHWdVTZ3CdmHRTvtQ5FzM4nNQ

Kio5w1RpXQmlqyZ9zvnPMRH8j5Sp72/NOTio5AKgUgoauCyFT7YXwsRQgZFAGHDo

uA6B995yHB4shR7Js1KyWCXJU2SlYAkNgFak0gIDam08BbYOg8jQcTduhL26E/a/

QBP5fMdMgr7DCrmhgZYhB3BkIlc/JIxIZW0N2pGVISRvHdNbGq/IGqbnaruiQayF

AajMGNckPoxwOhfGYBQRoABFTExr2yOs+qo316jAZTPdTW3Rc7DPoD9fqMxi7UBW

LDXYmEUbsauNjTCBICb9aJEZSmoJsQE0s0zUylkHTMREcmcDJJ+aJTpPlJkmLZap

YVsKVW4puivQhkiayUkZpSS2hVnU9WeswlmiJCSfENQ0gRbbQIYdQImqsmhISYkr

IJ0jM+ObcZc6ZlFiXfMp2yy11rJnZSiA8ctmNWTq1A9InIDHLAwti9Enr0MZmkJY

q0AV4cYoZGKx20OByucESDpLJYkcPOuJ6516+HVACXEIQaFMSV30yo76RjjNuoy3

ol43qrPQ1dQG745j+sOdDTY5zGNXMuO4G47g3KLSMhbXaTN3LW2ptQEwoLBWKbHQ

5DU2F0XS0pL5pKQtGTnZJfmnk1LS0QTVp0RtOrUhiudwiWGAuETmfkh4NKydXWxm

bqJ7bBdwbrPLsWau1ZBKxsbITtsmb+zD0OEW9BmFquVs3eLnXMuFdq7XAdBReuUB

G7N3wMEu+iCJCKRA/gS4MBUAtC+L8xeFAEGr1twQB3TuXePsLVfTOzgGZW6gL/J+

1QjDFHp1tJgX8f6P3/lAQBGRgFwqD+A7FMHIAwL9/AnbNu7c++d789bTGtsd126t

RrrJeM7XocHyjrNKMbUu1w1bOupPVCEMhYMFBshKjez66zX2vXAzM0zyzH2XUmIo

nZ8XjnIcRo2jDmNaIMS4YTVGCmRJ9b+YovYjp9MfQJCaiGOK6P5t/eJ6qWL4p4vF

qybf5L+TK0M5++1oratO6mk5zJcpQQrC1WRuDgU6PWY+ou+Y9mLwkuFESy1YMuG6

cuW6Cu02uy+6acGcWc6uecl6aAmqJceuVcweIBCwZcZuLcnqFEleEg68ggHABAxB

xAqAEQQghwkIvgY8tmS8BeXc5CnAjB1czBrB7BX8nAXBAehgQe3AKq1B98iekeCA

0eHG8eBA4eSeKeIIaeoCTAECOB0CG8ee+AHuSC/BDBRAQhLBbBG8YhHAXBZeIIIq

80O2dez87UPGIIh2x23A0SxsUS1Wbe6q2uN60k1QHAcAQwcA1k0IAwXAJcTqAONm

32HqbcEBOYiRo+c+gaoOliEOaMwSjiFE0a7m6+QIVMdIjMTUNohODgh+jScUvijM

kW+iDw1OEAqScWFOCWVOJOuS5aMsaW7+KRWOLOHaJWvAViOsjUxs3KzMB08Ygu06

aY42NsOYfWqYMBg2K6w2iB4BFEk2O6Suc2WBJ6Z6FyWuV6neDgJu+uJBhBDcTclB

qRch1u6Apwrg9sbuJhEg7xywnxiyl8UhfwkYoe6hihyh4qqh+AYJ6omhFE2hGeeh

p6H6KMhhcCxhvBEAvx+A9sDh1BzGlerh1wrCrh3hQI0Srk5GcQx0gR12lxIR3sEA

XwMAWQUwuALQi48RBm0+RmQOIuJwE+VBUWEMGRfJHw2R9mi++RLmWMsOIapRvA2G

p2bW2IbKhGLOh+oSZIMSwYVoVJjaV+qxCAbRHR9+XRj+bRGoKWAxMeDgjOCOV+Yx

7OkxDWvAyQzOFMDICxiYQuyB/J0yYuYOmx+YQ2CBbssuyx8uU2hu6Bs2mBRyx6S2

EAlyeBPCt2uuZgtxhu9xpujxFuQpX4mJ1kkI/xc+PBrxEAxZUApZDgsogewJQIHW

y48hf84JtpoQce38ahChsJCy8JZcOhmeSZue6J3x6AVZuJ5UhCjhBJLhnhNCkqaA

0SpJ/GaAdoEWIYOytJ3CBBt01QCArIxqVMLC2AQ+ops+wpApP2U+UMSRwOQaYOUp

4aBRq+JRFE7iNIcQGgTIFMzWTCwBtWmOCQjSAQ7M9aISloNWhpUyJpZOD+iWvRVp

r+gxFE9pS5v2TpsQLpBcgYTURILW3pNUvpkZaRAZUB4uwZfZtZ0u4ZSBJF+x26iu

sZyuV+auyJ2eyZFx+Bkm1xRBBuScOZFB+ZzxzZFZpojQuGqAMALA3B7umJ4lkl0l

FgkhpCjZoJPZ6AUe7ZDAnZCerZvZQCA5iJWeGuOeaJ5hY5EAClNQUlMleJs0W2Oo

2AcKbmVei5qAzROlnGZJvArCTIx0AY25HeDJOq04EQmAvYIwxqg41kvI3YsgPA7Y

zAyELQlGZwZ5PJI+YpLRV5wxv2Rpw+gOF5kA8+j5eRz5MpTirl8p751IaQYS/ijI

VofohsHomOjQ7U5ouGVozWOyVS5Iuaz+pOBaAJlOyylptONpRSeVmsX+naqAdof+

hu7Wp+DMBFAuPpSxs6pF865FQZJYWxUuOxtFCG8wUUm2zwhJlkOqYw04xAIw9Q7Y

3Y/Y0cU5nswEOqnYPYA4w4o444k4M4c4cQC4EcpkYkO4scKB0ZScQQ/i+0cSzWJx

SZKZwV9lF11QV1senGCOte853lq5mWAQrIxNbWJIQVwRd2Egt191j1z1GVt5mRl5

Wi5m+V/2mVRV/qWRIOkp5VUOkaspa+tVBMSQXm1GTVoFF+XS7VfOoS3VRNzMlSTK

A1pFsFI1cBRaCFQ1fR1pBS2laFCqGFbO3Av+Q62yHIZo/i/OnWm1exTNu1BYFFB1

IZ2xYZ66ttkABxTFzI8QzMHScYCZ2B7FplnFVy9JOZWZScTSbIdoFM8QMtVCGZuZ

5ulujhmJqQAA1KgD0DADoHAMnjoMwPvC0ACBYuurJZZRnVnTnXnboIXcXeUACGXS

pdIe6E2YWYtDCS/LqO/JCV2dCRpRAM3MQMQOsKnkZWAkiRAGFRFVFTFXFQlUlSlW

lacPqCORZWnXEJndnbnfnXXSXY3bLGjcQmDVQESWUaQV5bKgTXzmfmytEh4dqmJj

uTxaER2F2H2EOCOGOBOFOLOPOJyUotyQzdldfg8IKSJTlY8OzXeVzQ+bkdAkvi+Q

LW+XjBvjiJrKaE1e1rCNSRzAFsLZviEk1BtOTCyCEtBTfuLMNZ0aNd0eNYhZNbrd

NeZniE0okHFMSKfo1XFMMu2kbWuUTEELhhtBErCH6CzlMVhd2qdg9oRd1sLnbesX

Ms7Uda7aNvRQ4J7Wgd7T2nzl6QHacSiZrqHdxemYBqQA8k8m+qgFFCxrRZPdPZFd

FbFfFXAIlclalZiOlXuEybgLMEuQmtg0wuRqwtRnVmQdgAPHDt+ZElaBVkSIdGkC

GPsNNJAJ8q+o+mgLY3kOupPXUE0K0O0F0L0IMKMBMNMFRaAX44Fr+XMb2oEAEviH

g8BKXJE5YsfviP+a1IzASMyCk/8ncN+pCL+vilCkYwcEBpiiBiZWikqBikRFiiff

qHBhGUhphqSmAKhnRnuJhqw92jSCSFw+yDw4JIjkI/rC1uKOI7RvYPRm9YxjOY5Q

gM5cUW5Vxq5CuQ3mtAGMyK1MrY/Vds/WY9FNUMSGwDUD0MwGMJpgzC0BwDAN2EII

pDaoONKMatpR9O9sA8VaA7lSzTeZ9iA6VfAyjIg5VUUdVeDgqblg1WjkmniLslfv

Yh1XaN+SzPfQTv5bjf6arTQ+rWNSWlrUhXTsw0zqSLiCLRUp6MzPEKMfw6gB6Gwx

SfrP6KzLNabTJKSNST6Kwm3UyYse7bi2RQ7ftbAdRcdW7YSpFNdTFBQMhNgI5EMF

eDoKgnED0L2H0D0JgMQJiIpC9YJMfSFGZBJO9V3hID8EMP2JIJE8SMwH0DUCYJeM

SFBOwPpKDUG+DSG9a17DqoOCeMvHAHAIOEIPrJgCMJpgEAgMarJv2MQI0OmyVFHB

Dds1DYccxVTIdCzmxWccY6mZqkfU4dtogmfZlr9l4QTc1mSCkDUYeE/cFZTV3JID

AGwL2MUMkJUxAJi4VTA0zeAxZjteeZzeKdzQvrzcvoUQ4K8zVag0uV1UMiwrGNyq

zNSdLYzBaEQwct7U1C2oNVQ+0XBeaZrX+8K1NelnlSSbUt/rEBI66SzB0rCFTFfm

AQo5A/bdAU7Ru/ASsidVa2dTa9UExIhLgGwAMFMPQKyFMMhF8GCO2H0FAI0IQI0N

kA2xAKVK9WAKkxtiC+G5IJG9G0ILG/G4m8m8kKmwkCx2x+ZNmx9fwtOJpswJeAMD

AKlI0N2MkAgNZBhP2HiuCBJ021m3hzmzFJXKuzUBwB0EIDANgF8M8oOFWB0E9XEM

wFWMSHp8G+FJJEZ9UJXNVoQMapXJpi0IpP5AkD0GaF8JgHFexL2G55mzHC2wxagT

Gd7UyK0m1kjfoecSY2mVcWQZmcQVhYJXmSnS8Z7kXpIKgNkIQLgE/GwHcN/MpaYu

WWV97hV1VzV6gvV5MBfPWcHlfh3J3cmUodpZ/H3YN8nhuwiRPTMwYbAuvRWV7vbm

19V7V11xYAO8xmxyOwzHq+O18/4JEi1kwhIhfWdO3hTXuRIHaw61ME69eK6+6569

676/64A1i4Szi0aXu6zQYoe7ZhKaewg9KdDsg3Dh5o2TUE0gEpw1wyEoEOtQfhiB

EuaM1m1MzBFsQ7Nr+zkqafzIBz0UK4w2/qhT9jwN6IGIkLlsBXEpw46fKwGN+Xlr

hkwgGJ6ISDBwXIbDSAyDSRtURVtSsd6kowNio3ATRZa36Zo4xWgXunGQcqxYmZl7

2/O16nelY5kzYy2HY7kzFGCxC1CzC3EHCwi0iyi2i+2abNUwE5aGyi1m1szEyO1K

QRE1EyUu1NzuQ+tF4sTXiP08+l8ur68krwtl+roD+mCqM0mXchY/MxQIs8HxgHM5

BjFFtyCCs3RWs1JBs1s7czs1JI0EkOT+6ZT5w81lT/NvYAzySLCMz9I2z5aDc2AH

cxx9OfiRXnOVjXtu6GOwuUdgTU1GaOyJrCHqqoCyr2G+gMSB0KcBQJRj0DwPTR90

e5A99wSzPsvxAMS2D0DxVSD1VXKVS0LbouUmEgyETe6Swu0s07UdSKwvTAzETWl5

mhFtj3fnj7QxaQw/0Uw2Byza3pB/NRNo9Jtk7IbxNSX9rW1+ehrAqsLwlyHUxeFr

dRttUS7Q1CasvFihlyDq4Fx+vFfLvxUyxFdk6BZIdqvB6D3RmA2ADeHnV8CoB06Y

8XAKgHwAMC8AFjcupiTIHSgKBVA2wrQPoGMDmB9yHrkCWcCyFRKYeAelpRUJjcB6

E3QyiAmMrDlzK+eCshwK4FsYeBdAhgvwNQAsC9QG3dvsOzxpd9dE1/DsvjX266If

QbWCnrw1nZj8LuE/CAHAE0zQhTg2QfsAkGQiL91+JmcfNeQPbQNGaJVAHmVR3580

V8oPa9pAHcQF9KMTSDkHiB25Uxms+Idqg/wTRxQzQCHO0FUVf4SBce5OD/kBxyQg

cf+QxFhg/T4ZQcDuS1ckh0h9AeheekA+RpL1Q6wDKKoZbDhLw0Ye1peMZdAccQMb

I0uKOXBkjcQK4CVE6QlErmIOqDWQdQxgQgEID0BfEiyCw5uMsKEGqVdEerAbhIOG

5SC9KEeAymPXkHTdFBc3ZQavHmHhgNhegfQc8CcouVYc23BOp3z4wWDdWpoHBrYM

yBzsHB3HdAFWAGBQA4gFASuGYFaBDB4I1kRkC0GYhsBXsXJd7j4OSL4sAh2LDflv

yiHWJge/NffoLRvaoBGYOIK0AGDwzNZ6UaQ9rBoE9ASJpWuWGHuUjyHUMzSRQgns

ByJ4oU7SP2bELSLtD1DyQR3VIQAPGKpBcQ60A5PiFcgSI4etQjyrKMbSIc5GxFZA

Yo0DIbEMOnQkbKs3GxaN+heyA9JUIWyK8sB56bLv23uYbZA2zhQwe8PcobRduvfH

yrGEiRE1qsvws7kETDqXd0AEXbIJXGQgdBNMemZEduyCFGtV+GIpfv9xPahDSWeI

iIQSJQbRDjacUY/IkGpK+hSQnINIUEG9CswyY5SFkEEAkYwVeiBQ+ChyJKFci9aH

+X4ZhRqEasfCjIAMIkH0bNDVRgvAxO0K1Eu0uhSA0NoCNijxREoyUVKOlEyjZRco

+UQqAG0HaScSIkNFAW2wGEdRjRyZU0T22WwWiX6m7PincSmHFdiBNBdAFV3yBZ1D

AxQJ3PvWYBN0muclCsueKgCXj8A14+uqXWJ61lAS2wkfqV0G6SDe6RwjQpN3Hq6E

ZuqJS4RiSfGEALxPQK8TeIbp3jD6Vo8vJHD/Ajtia3LLwM6P75joQkZMKxF6LpKm

NcujJX2GOIDiTjg404sOHOLe7hiQGX3fwf6UYk4tsRh/BwE5nPavlt+R/c7BaCv5

X9d8dIi+syyZSNJWEnVbEIcyAJljKGOPADuyPoaE9v+X4yAPrR27H4ZaTKVmG1iZ

RzVxiEWb0EzESDxAkgnSX4ZIyBCZCPQEtPVsh1aFGs0OjtM1jKHF5IC9RfQmGmuI

OQbju24zEOn2z3HR9LGPyDXtk0QKT1/RgY4MaGK16tNXeDmBqi1mqTUlueeIT/Fr

y+BW8PKFodrO1jagNCGYBIP3hRHSaB9uAkU+xjFHybNA2gnQboP0GGDjBJgMwJcP

uLaYYgcQp+ZkKqQRriMqSPjHKf42JF0hGQMPI7gSDay5YypquUPsCmGYR9/0CfUK

bH3j5mjypSfKZlBkbYYS0+ozU6lSiz4oYKUefYjFpOAo6Tn++k8JmAGMl459m5k+

Hm1Eb7N9poaEjGh3xwnY1GEPfcwfKgZYchNYV+YiUCzIk6pcA1kMYAgExC4B+w04

bwbyU+6mYWJdtP7nDBCEksuJZLPfhSwP7w53QTIcaYzCZCWhG0EtdquK31jrRcsz

WS0HaHSAq0KxSk/lnQ0Faci1J3IjSRllAqGTnS8o/aMBQZa/DHJPQ5yb2LckQAsO

OojPl5KS4+TDR64zAduKCk4C8u5cCYQQKPFECIGJAuYesKWErCHxllG4YsM2HN0G

yOw9SvpU0oHCgJ3ZW2QAlAlnDwJFwowqbMNmbCHhBHZ5s8NHpGDq8mWUwZfQ+Hyo

/Qp+SJFTCIn/CfRjgqYAkBgDZBJA7EHoJXA6SVx2wUsTAIZGnC9gdAXgsMRjNIpR

jWJxc2BjkT4k4zExF7SAFe04mpiCYiHOkC2lJlN4GYIoxHgTB9BhIOQ+IGkCVOIY

bijSvLNkWzM/6qSda6kg0D9hpDk8AkxDQ6JRhSH8zjaoSGJB6Gp5VIlUMSeUaTJi

SNoWQKogXr1g1HKNMOHk1ZkONfqaUYAkgKKv2FODWRkg0RcFPgEkD1BuwkgCIIOA

t7o0M2ZUDjsuKl4Ky0BSsvySrMCko1giPs9UN9LMHGDdJnzeVNEgsmhNQZsc0iSF

RiiXh6ASbUgMSBaCaYkZWVFGX4Lypr9kZWIrGVXJzy4z8R+MwkY3KHgsIcQpoTho

73ahkgiaYkjfF6DIwyjCpjI2+MzK1qVj8eKkzmVPO5kzzhiuWVef4D1bWSFUBU2J

mkGPnQCheZ8kXhfMQG6ioyq48BfIqGEJ9oFcc3AZrPwEhJaWEWHfOtGarYSNZ0wk

8ZiX5BbBJAruE2a4rYDuLPF6tXrtcEcUkCAJ9svGlCXG5wkHAU3N2QnzXpXDqgbi

7YKXlQmPN0JYkbbp6GQV9cm0FMR/uTXMU3zkyd8h+U/JfnQg35H8r+T/IxYJFAhT

E1GRQujGoj7ylcnEdxKQbJjaFEAGIcBUkkMhcsjvVIHTI1IYh9YYSbjM+wZBMoDY

LI/9mrW/ECsn8Ui5CnWLypxJcQmDBmGygJDNY9WjY4kUTGAqbzNY+IeIBj3lG9TI

kMSLKZ2JPk7VJZ8A81mo30WtsmKvk4xUekDqqyzFWC7kGr3ClB9NewEHJrqEnoJy

k5KctORnKznMAc5pAPOQXI6mJTUwJGC0OSB9DBhzaVMRtDSGGm5SvQ5SGSfiDMkH

JqsMSOaWkxfSVTTFC08PsQD/SFJVpcKGPsnyTIQYdpKfKOLikOm4djpB4bPmdKJR

SRyYGylvCyACS2826lfQ5XjmAo0gSYGPN6bAtkh+zXm23AjFkrWhTse0TMgFudwK

WMlewxIXkO2EIAJAxgxIKYDoHBa4B2I3YOAAEB6DF0alQDGMWiMnxNKqFsYuBl0v

aXktL2lLQmUPH7lQ9qSwYLCf4h1KY4T+7PIIEPx1L0zZl4i5SRzJrFczVlLNfxAo

o8rYVGobUJIAMsWp88Wh4smAdorgGi8nlA4q+dJzb6XV4Fw4gIJpmwDIRpwdnXqF

J0M4ydqgp4c8M61vD3hkgj4Z8K+FwDvhYugCiqAlxAWoDd0Ri/yVuKgUjDLRLfIV

Kkq+l2ifpiCxIJqvdD0o2kkSfJb8t9EQAm1LattVWF6hFy6lZCsBmjNQ7lzj2Pqt

pfQqTGMKUx3SjEKkAJXtY2YEa3LAEXwZY4yesaomhuWqyJrRFf7ZNePOKEY1axor

a4CwmzVMJ5R0SB9lKKsRiy1RbQ8tR0P7GyzDW+omGjo19p4grEAUjij8tGHh0tZT

UJpC0m7T440gVgq/CbmcV6zTxEAEQjYRoFjwnKYfT5PVF8DMAx4VYTAIgAsaoACA

1HNgRWR40cFOAqAATcCiE22FC64myTS+Jk0AN/FwghHLsJbLHCu6b8YUA7P7pOyh

6I9PYP2VdlDkJARqk1WaotVWqbVdqh1U6ruCr0lB0E1eApp4Eqa6wCcETagE01x5

pN+AWTcqtY5crA57lAjBuL27yoQkA6NUr9jBnqzGSvai8NeAHUPgnwL4N8B+GvWY

jfBd6xpWXJvXUK4x2MuhTXN4k4iYhfOc0HEjDUzTfyUaoDazDpABJrQgQAjK1AMl

QbFJ8ymUBrWrHwb01iGmyV5llakqyQk7UkEEv2VeZjoBGGvu6X6QDIOejUT0EATZ

TqLi1XY0+XtU1FSyZZuxFDhNm8mE1SNBIbnJAqo3LqQpzKsKQ+kBXVSde1QRzaav

NWWrrVNQW1fasdXOqOpI0gsi7xRU9TW0ByK6eUmywBAKVifAPgCqqla8QVZgPJg0

HqlFMmppTVqRUzB25SvyiQxmMdCKkchU4PjZFd1LpA18yGOWcAZUVZBI6mBgzMPk

tPpWR8mVkzBZtMzZXbS+du0mLftIojp8jpGGE6Vn0FWIZhV7pCohBsZmLa4kgkVb

Z1Q20STLQEiGoEqpSV1qN1I3XvlKgvpJbnAxNCLKl09GYKaNJ6qsH0AoDTheQQge

oLpuuK1LSt7q4gQVUfXBCatvq19bXNY6BrweLClrKfyEwr5BM6XIDay1AqgU8s5S

btFEiTWsyFl7MpZWmukUZqdEfobNRzmbGMIWsEiNIP81AIGsrtZa07efO1GXanJx

GsBRgQgUmLNpyvAERrIjrazjc5BY8ZxvYHkDKB6g1aKsJUH97uBQ+gEgErUqp0O6

+wiEmEukFOzZBpw9POcNiU+bLKqggfdQMlTRanhaquLc/EA32i++Fg4hrhgpKtQj

1NuxwXACrAJAvgQgFoEMHqCJQOgUAIQKyEIA8B5kiwEhRzTK14sPVlWj3S0p5phC

eJkQhuZ+oJjUk6QTCH2ulNdGljMcxsLzFmPl7+VT8HzYbW/0KGwaJtL+EVr/yZzM

hs1Ra4AX0mNgBgIkNysvTbQr1aKq9OimvThxnTXybR+sgNjqj6CXgPW04EYJIEnW

edu1YECCFBBghwQEIKENCBhCwg4R5xm3fTvF1z6vLtGcNH2iTUe3B1qNK6j6eurg

WbqEFQcgkLuqxxHRyMIYIJRlrb1ZbeDfQfg4Ib/07sV+965yT7s340KX19WyA0Gu

aBeh4eCBvuREmQNAbjYXodA8cxxUhIlF5YsRanrG2LKJqU24g/tmzVADtYsHemZr

HazGwNFDBnsXhr7GqNq1csgxV7RZCmg/mcULQ9gJsPjCrF/C8rKTrahtjGQhAp4v

u1K5zC2AbAZggADIrCohPjRwH7BHBiAQgZyo1zLKPjrhPR/o4Md41KbRjvRiYxuk

tkiDDNM+p2a/AKBmb59wE6oFZoDm2aV9MS9ALfvv2P7n9r+9/Z/u/35hf9oadfUW

TmOoABj/mmgcsfGOTHo4eh/XQAreYwgTduE0/RKuyO+0r9u5RweBEgjQRYIUsKQ6

hHQiYRsIThiMcxIq3oyqt3q1pVAb9V4yA1BMkPcbFpGky/yuSgjBSUxzHlcQCHWE

K1BayMg2jOB/IfEelnjbJFmelZdNoVZk8Awf5TNL1rZR4U0jjSPnH5JSGNVlyBe1

AHSNjAHbRZ5epyZXpNZnbHl7kvRaUdUPJcKj5GLhV20XVPbdxwLCZhYwyYfb0dUU

mKBcYf1P6X9EQN/R/q/0/7CARO0aaElZjorgZxzVnqYMh207BkTeXCtRiExI6Kpq

OrJpaZqnVA6phTRqSUxanlN2peK0aZD3ZBO8NyfOZebvISnDQkpqKqg/ElbEDaSp

7WVnbSs50MqxmHFNaayoT7sqhdnK0XbBh5VsHZd/K06WhnOlNhYw3mAU7VnRUin8

+ByOkAzDpkW1QBxIXXauoeb/GVVLzVyphN+Gm7rg+sH04d0hN7idUzcdiPgGyDEB

uwikKYHEEEM6AWgkgU4AEEHDMBMQruhYO7rdUlzXD3u7E5jL91eHd+DCwk0wugO8

BmsZWRlIyEdF4gRl/gEMBaEv6Ux3SIYYAintG3snEjX/LPTyZCSNI8cRIH0IEGxC

Mm0j6ad0cdB9C75ia8QQWSE0CBE1qYR2u5axIeWVqNTzyjPuwZ1TURaI9ERiMxFY

gcQuIPEPiIoi9gLilD5UYBb0NAVzrG9Hy1XIae0PPa1seuhyvWsMOhz3KHIUwzsl

JrXK9W1hg1TqmwDGpEIxQBAAMDYDWQ0T9S8heiOAOPmK5YBhMZ+bfXfmP17iNlET

EaYYNGYzIBbSgciS4h3SlOoRh3LNBwW+WaeiecsqIPlCmciNUUQLJlMHNcMnbI/X

QagH5HICqp6vQRtr3iz69YluXhJZNFfKl1xpsifUcPFd6Hiuszo7MIkAfGlNZAwt

pduH1+brCimjgFnRqhwAGrE+/TVPv/Gz7DdFjBfcZudlyDTj9mlvWZSgmWUarrVu

qx1Zw7RbU+x+42izhXOMISx7SDkBgvsHaWYoLFuiAxCYgsQ2InEbiLxH4imXb1gB

r3WzRAPWXAetl8IYHvrlBrHedO83VaDCPGwQ59iFhF6BKnig76XvMkEFbHkhW4Nh

B0DhFZhDmgRa65RmM5YVrZr/zZMVhAy0iTHR9YgspID+T0lYalTpaxg2leYMZXWD

WVm7TleVwLqCrRp4KSab+z/L3taO4FVacOOEBdz+5w88edPPnnLz15280irzPtMo

9vaYmmSJJFWgUzPhb8hNI9AbzYw2DHgGGapURmgV5UlmxIFjMNTimzUspm1Mqb+m

3erCWEJTv8RxQ8QnckZLlKZTk846zOEWkynaTln2di00FFzpWnjX6bLKjlQLuIDr

T+dAJ7lfBl5WS6Oz0urs0Koulmh6YfOOG/LZzHO8wAjQZGzLQ+udIMbCQac38fkv

VBnkTcTgBv0OyrngTAMkgsBVJhjnNzdNnVMSHbD1AeAQwXkHEAiDv7CAMAREB/sr

gRA4A/YU+gxPcMYmLLWJ260+txNPlHrDWqA+4iJC0j1dDMv0GQyCXBI2U9MYlXKo

7an5h5sR6DWycFjp6kjyFlI2UQbHytT8Si10plLxzYgL62G7salfQ7nbL5jF2tdn

YMN/zGSAwYoNkEHCkAyBL1ZtioZXFe11DpoeJDUfNG03Jocl/+baP6vGDRBRh+vM

lpDARZKMOQyuxDJigf2v7P9oQK92NwPnml/pUuUPassj2bL1cuy09eD3UsiY8pmY

paHnt85F7huL8vKsp7cZEgl+lk6yPf74HOTk2g+1DbQDqsKI+y9I/Vi5y/MyG7UP

I8qcJv331T0sx+0RvJtCNgHcSJoZJepvSWirYwg8YbiaSZpl5sdNqLqosUcbKr7d

VeFVxdxwp8AY8fsEwKgClxSAxs6Y5ZRseQgCADjpxy47cffjJ9CrTY+IO2Pd09jW

NcJQPSOM2aolYEsaxABrt12G7Tdlu23Yf2shO73d3u5BI9mYlPHdjnx/VD8e/HW+

L9+c/7JicIPn4mSg/T5XFMMO3Rp3a3VCeHHEAAg04IQEIDYCYAhApwI8u4MwBXn2

IT8IQFWAusb8B7QBkh4Q7If3WKH49nwyHuaCehvQq1dBTD3ax6t7E/idNGkC4Vky

RabKEG7w7BsEGacyRoRw4nTGtQgCpt0vrnuitrQYbqQMNRboiQx2dtMIK0GvaPlU

XNFBRpgxWt0UMWVHolo4srOb3fKZLkDmc9aMHaY0t1QcyOaYYZBzFTQvzvVd6OPW

OCKOcASuP2CgDGpcA4zgA8zSmcPrXzoBuZ3VsocT3fDDRMJAckoypAzlmsHM13IV

bgXas9TpNJ5RHksz4Lu90K1yfCsk9hi0SKxPspnYvBYOsIK0IAWBt/OUraxQow/c

1OgvZ14LpvZ8sMY03MtJVwrjrI6O/YuNiEFMtsFASoBl42wVBIKGZK0DUA7YXxX8

FQCDgQMc8F5A656C2JdQhARYNgCtd5InHNAgY1MDfgKhhNSmh+NRxsKSB/HJVZrt

UDNcXoLXmcK1881+Dcx7XmdJ10kpdduv1wHAT11vR9dmA/X38QN5qGDdKbQ34b8g

DwOjcwJLg8b9k4E6tpiCQlc+iJ4NZAkjXBySJVWXEt81JvzXmwNN9a8zd2vHcOb5

17vALcevEQXr0t1/H9eVupY1b1q7W4KARuG3kWpt3G5Kdrq5zm/VVYuYP0yEd1tT

/vq6NVIdt0H2C77TAGJA9ATw04egH0GKDGp+wYwPoKQGyDdhEILQQgAahJee69ZL

54e77ufV4mA9dLpZ4EC8wBId8LabhVs+Nqb4MNpKlfO1islb2RtwVhI3vaQvcnD7

sp+XXFCSDHkjbRNFDWT1CbwGWEJY3whcrmIpxGYDk/GzholmqvFHF20m1x+ytau8

rm47R7UfpLRaEXVTyMMXavqn6YkVMWnuQdEzbXsXw46EKhE0w8BLw3YY1KB6fOYm

KXkHjw++Zg/eHOljWpHrAepLlI4a2NoqZjms9NIFaq1XLME03sKTcDVY/hxDbKFi

vzMx0SV/K2lfKLXIMdI5Rx/oNyOAXRNoFywe6ECfVH7bM5RRqktifVP7erWfnrKt

J1jXoeaoKcF0CmAAA5IXUW6XBUAvIbqH3TeOoAvjqx1AAMH+DuvtgDriIAgAoBZ1

l35bgN1vVMCCBSA6bu4JVwQD0BUA6ARq/l8K8cASvqAMrxV0q+ZxqvAxur85Qa9N

f1wLXzOm146/evYJZb1d716lhChBvL43IKN/G9dXfx/XIzf/EAn7HHZQ1pfVoTic

DvApQ7yygV7rAze5vFXqr6oBq8reXxjX8oBt4q5bf2vnXvbyu4reHf+vJ34b+d8P

ezmynJ7hcy8PPcHcQ5q14kWzDZS5Ytr+q9L4ySGCYAEgvYTTNkGSDBd8AmmY1L2A

6AoRoQVYHoNKCRF93KXRD58zddIdQfR7Z7Dpe+q6U9KrQdO30OTuTSen2qst1Z4d

HxD0zvhlFnlgK4I8IWiPk8kj5c6ZQhz9lSniRzJG5ztI2UWZ2RwTei8KO6LSj9V8

HfOocHJPw47sLyAeqnAoA3Kdjpxxt86pCOxHUjuR0o7Uc4AtHejox2Y4KGtsi45Q

03zKMy9wF2IMB1lwgcIAJP8CpS1xkvfH6XRSQAdMBXvcLsIADvp3y79c4laefkYr

n79w5+zP4x8ziA+Z8nvUhxRf5RDseXP01OOXyzr8m5bl9UkxGMRjz6ycFccnU1Aj

jX356Zz/9RH9PXNUhpVakgmTJvrjyqfN/AuSjGrttqRqqMK9RP4D/V/o8mHZeLHJ

rzEqasoGcBmruofeKsHqhRMJvHYb+EcH6jsFz/l/9/VMYCfdWgnNsoazsZ7r3eLN

Q16J324KCYEKT7k+lPtT60+9Poz7M+rPt5qTWR/nf6n+j/mYBZ0cKC/5I+cLoobN

mUnu6C6+Kfj5SzYxsHEhVYOfiepe+JHGRwUcVHDRx0cDHExx6enPgZ5uGFfrz7kO

NLgs61+vhmMrrQkSAybyq/oN9ZfqirJ36J61WD37HOeBqc7ee5zoI6j+1wOKJMo5

GMXpBm7IHsrysDILSLhI5FgdqtahpLBzWgOWH7Tz+t9iq6Au+GsUaEaV2oJ59auj

Mb6QuhVgn5/KZptSqq24GOrboAJPmT4U+VPshA0+dPgz7IQTPiz5s+wEODoQMBts

lJkg4agRj6mm5MKZK2KOozaRmzNtGa0ES7Cuxrs+tr4yjSXmI6LFifoAtqwg+PtT

qC2tOu1iWSIDnhSFBLOh5yfoztnSpVmUfK9p+2u0qrINmcfP7Z7S6SgdJB2bZvMD

rMnZtOp8qTYJ+T0wqOEoGswhKlKpgA6gdEgsgWgdgy/qmdqU7QOnBhj4Kiqlnuj9

amFiQGOCHQO+5wAIwBQCHk9Abuxl+IpMwHGe0HmPY1+gvhZ7C0NLJyCoOmsMy6n4

zDjAZzyDLCGDMgGKr1S9+rRMr6g2hHsK7D+orjyJ5UHYlULzUWXhkac8pKmQwQhp

sJx7GBxrEv5xenklH4GijerH52BernUa7+neuY496ljvrISAjyGwCX+pAPUCF0w8

FWDTg1AKgD8GAAPz0h04D0Byaq8GSEUhVIXvC0hLISMDMhDIWyHrGIJNPohOQ1nd

7duBxicLPedmq94cU73piSchzKtyE0hdIQyH8hLIWyG76p7uj5LW7oLQZVOLohjz

y0OwcOJjApwEygjAOgH0BUh0IpID4AUwIpA8ALQMWSMwJwS4aMBEHiX4cS1wQL4O

WQvjjS0iMxMPxWgbPBtC8KBMJTASsz7KfaZirfkzSjyJzkCHg20gSP5gh5mARhys

1QgtRT+jZObT+UGLklYlqC/vI6uSvHso7W+lkPC4NqhSsqDdg7YHEDSgkgIOBu+w

ho4IRsUbDGxxsCbAgBJsKbGwBpsofmkqTqwltdpguSXtSQpeW/vH6o0UDrb7J+hd

vqEHYIJslq4YcUAF4hyWlkT46odYQ2FNhLYcX4zOHoYPaGe3oZ4ametLos4Kk+Yo

yCKBp+KGHkgv2MywegIFOQycOh0AzDxhkDImESByYWc7a0aYTzLDETDmka5huiBF

igUlGDaBGBJ2jF5mBCAiC6WBiXuv5nYU4bq46ODgYnQd6TUO0bCUxIVxr8gZbv1D

CAhdNapFuyeBvCLuGCBwC8gfQO2D0h7xOuC4A0oEYDMA9IWSHlAUsH4ofAibhICE

RX8MREDwqAGRGQgMCFREVedEQxHuuzEaxHsR7AJxGsA/uJd4t0H/qKGDc3/uE4/S

kTpZokA1moAGr66AOaGWh1obaElgDoU6EuhhAG6FPGcARWT8RmcF05CRIkRRGeuw

8LRH0RqAIxHkALEf8ByRdXH17JKsLp9IB2awaGGmG7pAczcoysKPyE+1+sOIdh/H

IJw9hfYaJwDh4nIeFeqYHsSFehR4ZcF8+4Bn6F1y1Dkfw7IuICdwfhFHp6CBUQGs

0D+I0dvL7pmnDHzjiBXnkP4+e08vrQ4glSLMTQ8kcl7x082YT3IBg3Cq5DAUV/Ie

oympoO7w08eNpF6m+d9mWEW+fHvF7yymrtYF6mfOL8KUaGEZlqhS5pkzZq2KQYZE

WhaUCZHMAdoeZHOhrockBumCOAmil8noEyDY2XKMUFdSXaCcqRyvwZGrki8QftFJ

Bh0V9oSAevJCzQssLPCyIsyLLyCos6LLdEYw35PFYdIHoP5QgRuZm9FDwpGFfzfR

nDOji9oGdjUEXIFZq7YNBPOl7aNmPts0FNmXQWLqtmaYBHYkoAwQA59B+fF1H/kS

2tyh9R5DIJBDRjMofJjR+QVOZoYAqHOGDse+me56hwcqYZ6S/iD2gE+WLnFGFK0o

D0ARAl4BwAngfQNFy9gkgLyCkAVYL2Bjq/YNZDTgC/BlGkKEzg0onhTAUZ4+h/Pv

6pFRRJgqQVGgTMRbUGnVBtACBkYbAbRG5FkvJw0Watw5zKKvkK4phAEaCFARGYXE

JZiCHMcylmWYfNTEySHrlgsg3aLTJ/iMIZqztI+sPUKzRyVlF4LRprOWFW+vQcj6

OCUAHJwKcSnCpxqcGnFpw6cxLkOFLM/9pH7amisliFoRwwro5J+ilouEeIphr6DA

EF2DFFyxLToUqIQAwC0AtAP8vUCsg7oaX6eh3PrlHWxBUbbFB69sfxJ1RYGkaJcK

pMlL6mg35DZ5mgfclBF/BXMHEYD+iFur6hxsiizSaOkAFK5gRIjH2gIeEXjnHzRJ

gXBFFGCESv5IR44b5LYhOru3GYR2XthEX07GkSGH+T4hEAtADrgMCQJoWhJo6gAI

OPruOeTrAmZ0MCVAmaaCCXtjChPVh259Whwg969uy+v24QSXEs8YQJUCWgmwJmCR

YirQC1rFrix7CqpaoOxttTymhhSqXHycinMpw1AqnOpyacPQNpyowM8ZM7XW5flb

HnhvocvHPWIepgy0mWZlSTHkbwcGq9mTJp+R9oBsMfHGkAIUmGq+wIe1EyKmkuKI

SuPaLpI2e2fo87CO5oCxqD8DJutAvOFyoyCEg3aO6TZxxYciEuS+cUtEVhdeol7o

C60JTI4hO0TYZ7RzgZ9qgqMUIrHKxqserG9gmsdrG6x+sYbHGxqMUlI4gnDKg6Zx

iQESB0i4TFkGrmtIkKYkgAymRh4xVopSoJB1jOEmY6MUFGzLsq7OuwC2aMWwrZCv

mHHQeWIjpbbZBkPFJIhgYTL5ha6TtoCgc6RMdzoe2tZt7b1mguu0HC6i1i2Y9BtM

e2b0xYdoMEh2TYAXzGJWrORhmJFMBYkHgFMAmjy0ltAVifkcQEsFHuKPqLG6hiLu

5T+UGwXEzukvluwmMkxQAMBxA7EBEDGo5nMcCQJ7WCeD1AuAMSAcAcQK6Ymx/+ll

E/c5wRIkmeUiQSZ2xP5lPbWKBagbAXMNINKZt+7hEjh0yBamTKUYLURIptRqYZfG

aS3aFvhEgTvLlgJaiVrfFBeZPFUTfqsxMbCdJevobh9oxehAJFhx2vco8e3iYXFk

2P8TH5txpitC6J+wsbORdxRuknBOiJdmvL5YQQO0jPJOqJpjQB1kIODtgG7Fuz92

5seS6WxZ4TCk2xcKSvEIpCOCSZyqjKGSDk6G4syztQIFEwgBJcxEyYIhRrD+GtRG

eiCGQ2sgehR56YEZmJa6noNfZIhsEaiEk2K0RiEkaLIKlzMg0ev/HCpujrRoNGuE

TMJWO1QMf5HA0oOwAmA7ISmkIB6aYV5bCKkanFWOnbv1baRj3pEqQA0SmNaDu5Ca

vCppA4RmlIA2oWj7HGNyW4QHI9yTSDMwycYqm68fQGMAwAt1DwAIAA8NkBxAMMoQ

BDANQPUAu6b0GCnOGs8RbE5RmUVS5V+bATcH+hdwaHqeItihHLXKEUUyxI88aAzB

mp5SLsp0i+KSmpupBidnpIaZPFBEyS6NveEX0+yuKIw8MdrL5YWL/LFaYqpYp2kw

R3KaYEfxVahYG+JAqSnAE44/lo7oRaXqMKdxsDkHJvCraT5TVYlHr8wxyKnvLGMk

UwM6ADA9ACMABiIidqliJUKXqlXBBqV+bwpjltSBH4mfmfgSI0QVgbS0bUEFjYgl

oErTy0FDP8GnxgcYP5XpRKR6nphTOIFaWJExPKLhIQTKXqIhc0SWFm+i0cv4gZiy

V2qOCJnPeDmclnNZy2c9nI5zOcRflWGYBcXEJaDBY4WtHoCEGUKke2OhnuIGue/o

SEVW4CbWn5AzEdzDNw3XDf7oAE8HCgVggoC5mv+Y2m27BOxaQQl/+RCTKGjWcocH

QKhFZB5lOZ3mQ1zoBwUeU7764sSjHp+BNKjYP8R3D2nfa9AGeAcAo0d2BZA04B0B

VgFAJiCAoAwH0AtAhGeZY6pS6abE4mrAbiKXhHAbInY4xsNEiUpOgconpiV9vFbB

gpfJlLySXGdvZnxavmFYCZYcTogsIoSHowPJtngEixx4xIkBQ8zCAtkQRqUp856w

C8qTTPx7iUGlyZaIS8qAO0fq3Fx+reuJ5ipBgvBkOiEtle6fCB0AyDz2WWb6hDA0

IA5BsAinNVnlai6fPHLpd1qulNZ7AbcF1+JoAcnkgoFClpNQkqqBbEi5oE0zdoO+

CxpcOSvtxmAheicHGlCHURliDIeeptngRASBSSsISHIGkAZ78Wq6IRRcVxyFKckA

pDKQqkOpCaQ2kLpD6QhkMUATqbvqOFWBv8eZlQucaVhGZeViKAl2ZeXjZAlkFiDV

49AeKBYyF001lmmi51ZOLkDGkuUKDmACxi1b5pVsu25Fp+CeZoRKLsmFmkJE1rk4

VkE5IrlZ0Uuarmy59CVgEp+1wAql3Z8qDfRHcAXlboYZw8YyQ05SkCpBqQGkFpA6

QekAZBGQc6eiZEZ4Hr9n1Zb5mRlLxhqTIkOxmaGwydMBEprCMeaHgIxvWXCp+HxA

HCkNknxI2Txnnx42b56CZSGpJK/Mm1oKatI2auaBn8xSRFgcgRyjhGxWvhEnre0/

6TRY8p8mZlYJeYGXLx/xUGQAm7Rr2n9EuBaTG4EvAr2e9mfZr0fmZsKwiiwjimLL

g4nZSVtpDyZoF+LTKcMScUyi/RYSVGaAx6AJra46CZrraE60+VDpR0AGg7wJMnpk

yhHOy+aNJim60KhbBgsIN+oSIQyUMyjJ7tqrITJZMVMm+2dZp0HZO8yTWpMxodvy

oy6YBeskFqtImXkQUEWJXn581eY9Eqk8SA3mI6gsXRjW5VMa2lYUy5iuHB4OpFUS

uQrubFHu5OqCplmcFnFZw2chAHZwOc3YE5wucX2VdZh54iaRn5RD1uumUZAYQTAc

g3oFTAkM5MH6Cbk0tHTB7aR0Akz7pWiS6kEpfGSHETZV8Uzgi+02fSb7MB8oWms4

2YfHkY28nqkAWGcrhcpSiGiVFa3K/znnFqmvKRTn8pJmXshmZZ2TuKAJt6E4Eq21

SVABgqOGXhkEZZ+bTo5iVMM2iRqPvO6SS2ZRIzzhhYAscpBAO+a4V75ESUghpBDS

ZkE06BMOkncYCOfYq/kuGMaKW8o0nWh+UD7Jwz6SSHh/kjJIzN/mBSv+TMmG5yOh

TFLMgdqAVDBNKAzFNxSyfMBK040thaOirUN85TBOha1jNoPaAnrnJxcRwZXJLadg

ESxDuY3ja6npLkaDxJEphk6oxQDT4RAAQCMABAmAP2APQikJeDtgjQCMA8AEQD0C

XgQgCwVkuxGekQXBi8VwWFRRqVRmt06aEBZ4+9IHe41RJYmEj4UD2KkD9KkKbnn4

eaOUHH/hmOYYk/YzIIY7SM8QNXx2pA0VCFEwrPJmho8a2v0hBKyiprDkgE0pmht5

6ooBnk5X8aBm2Fp2UEkwZuhssHzhEqb9IeUiWgQUmp0cqdiyxCxeQX8ImmAMATAX

wI/JnFxDqeELxkieRn2WPBZumGwKUnqR+IeOA85t+EaVvgLy5IHFC/q2Bijl55AJ

bxn72gEUoVyBG4nfGCyRtjbwGh+rNJkeJtFp3n8eTFsZy+c/nIFzBcg4KFzhckXN

2DRc7OY3GrRhioSUxpFmSKnxppVrZm5eooSmmHwx8Jf5fAlTpvy8R7mb6WDw/pYG

V1k7/poV7CTshKFaRPbhjTlpEAJWnhZNkcbkOZ2CEfBhlcKAGXxZ+hkAWAmYFvgU

ypB3FgZMmN8X8Ju5W5iaUgpZpUFwhcYXMkARcUXDFzB5Zlt9m1Z4eeCkrptWoDnc

FdxbwXEipKRTCpSdKEw7wOEAM+F+g3oPSZgaY0dEg552iajm6JgJVIEKFReZNnEk

WpEw608xzOjjZqa1DhiHQ+QT6ASqzIt+lXSrGdSm6lL8TJkWF6VuYFd5jpW8p7Ib

GXfkulvOU4Wmmb2lUmxFNSRjTMlrJeyU+FJSPeGrZYapVgxIN8TkXcALllKJhqmu

vPaYg0RYkEj52vHEWpB9SRkFNJaSfTAI0xDMIo9U7WHkmhBCqN6BhqrSBhYv5Zyf

jHzSdQZWZjJP+U0GAFrQdMkbSIujgWQA4usHb9BKyYzGNFlfBaBpc9vPYr8mjivY

AdUPljwHiMLGpBRlJ/FWsnzABfIyCBM00kw5bxEiKrp1RpMjZ70ZCNGaDDFGAVdm

Flqkalmn699HZKSiz2egC8gMAO5CYgzANZD0AHJWcGXF0KVHk3F0icVFEiloGEjO

J1gtxiZ+VqRiBzBK2Uyho42FtUb+xMGpIGEp65VjnARVMN6nyiPVNqyaFN9vtleJ

hpaGnNxDer3k859gTv54CHpU4pgJIuegAtA54GwAyA9Id2AKR7APSGaYQgAiDv6O

gI0DsQG7OQAzG1QJVUVgNVagB1VAUWwCNVzVWYDLC7VZhw/iBaQFk65v/nrn6RZx

tWm2Rq8L1XVVUALVX1Vw1agBNVLVeNUdVeZce6SetuWuTLhJZcBpNEgBKQVDx1Zd

UCYgeLu2ABAPwKCns+RnqIlsFJGdyX6p0eRRkDlm6Z2wfFUOTNI082IKnnBqNIn1

JnKzaGyi4YF6Xw5xVwJTenugwmRP7Zh0ISymNklGKBR6kAaXqWZVlhdlXohuVRTb

7QfeflbQZ2/niHFV2ZEa54R9mdUCDgMAC7ocAAoVGzdgqAERAcA9IbyCMEAZUW5s

1mdH0AIA9QKggUAmaW5kQADNUzUs1kgGzUc1XNTzWmAhAPzWoAgtcLVEQYtcpGa5

M1bGWhKkoYQmJl+uSQnuyo5JiSS1nANLWy1FAJzUVeCtXzUOuqtSLWZpcGSZXAUV

+Nj4KBnaTNHWVEtRFiIQxqDACaY3YC5Vzx7BZ9UeV1frcWx5R/Klz3R62oKZ2g3a

KDWBA+UjtwSqbSBJIX0/LsuW/h6OUCUIapHmKWQh4xGjUyu2yP1lo2X4TeV7ZpOc

GmPl/Hs+UnZ+VQ4VqylNZYolV+4uVZelXRhICb6Y+pKji1vdYPr91mtcHja14obr

XxlUofNBJlKZdUWRZpAqPpD1XAM7WYS0qbJ7yoTKWcpKi3tUIDsQYwCPSKQING2W

XW5xe9VuVHBY1n4mP1VHVEiBfETQzlBIGaBk6gyAel8FIGi0Z9ocSCLR+x8pf8Ur

lSpcR7Eps8oF7Zh4jqXWNQkQXpUcpUmbeX6lHeYdlamx2ZiFN1RJRTUGq1mQSGlV

wud6U91i9dvqtWmggwJMCOgoIID1eDRoJ8CxDboIa5Gxp/63eE9WYKlpIWScZG1a

+stXVAg9fg28CWglQ2CCK9WsHYqvcbljkYNnppbNON1XxH1AdrIhBTAjEMHU/Zod

X9mV+vZVfV8lv1QtTH4TCIbDE0igTEgbiTltEgWg2pKGFQ5S+Ry4KB8iS1Sby7Cp

xl/FnnnIXKlQDXlSn4uORcoUwT0WIxuJXKe3k4lBcdYXd5BJSg0flhVa3Ud6JdUL

ld1VVkCKNA2gKpqiAgZV1WWUVYDE2Ca8TTQ3EkdDdUAaRQWfNXEJQAR7bz11QMk2

xNJgGk3RargGxjlALtWTRTFhuGSJVEpIN7X1A9AAEFfALQCMAngPAEIB9AhALyA9

AqCMxHZAgKPI2dlijRHk9l/umZ7A59Lkfg2gGNu+GvFGKeoHRgiccXx7OmdXh72N

l6Y42KFmkprC4gm0bpLI8uYiJnhVdIINKsKJ+JpUym/hSEyHaZhcq4ohB2SGmDiz

9oUq2Q9kI5DOQrkO5CeQ3kL5D+QgUPXEAmDpWGl5V+yKTUie5NTOEwKl2QpbXZXG

Ir5IZ/fFYLBGbYt7Wsg+QCPQwAQ4CM0XFUDO5WcFEdV5Wrxt9R6BfkraFBYWGdqa

DUdU60EY15YD/ObpJV0VTvYANF8bs0/YRINmqQtyiqBRBFzWIqa41Ndc8111OVUg

0txveb9jbRxJVZn4hjefv5lVODW8RTe33uVz7wYbtu71uhAIwD7w01pVywSX8F/A

IgerfkAUAQoFepeKFZJ97FepXuq1jwmrQgA7uOrbvBjw+rc+IcEJrW61mtFrek3C

OY9f/DZNuuTIIz1L3nPU1pk3l952trXBq11uwmrq1utzVjwIetxra1betUAOa2Uh

B1Sj5zJExZwymGmYUXr+I6GWQUSN45HZAOQTkC5BuQHkF5A+QfkAFB4tZ9QS0X11

Ln2WR13lcwodUPWRIgxIlGO0jPBpBjVEES3oJKIhYsYMdCtQ7nsNl/1OdauXw1+d

Zr5tYLciQy9Z1RItmdwjoi3LSSVgoGDBesHCEiSibIF43UW2JWTl+NeJTYVOlUrc

3WWZdNqEkxFyQfvkQAzTa03tNnTd029N/TT4rSgQzfRItMJQSkUMaG8sGBTtFFj2

h+m+SUCBNIbWDGAEg5KY2gVYKFb+VPtGFQfnY6cZtrb46SZkkWAdyUjanNU8toe0

0gxNCEWymBzQBrSxm2ldLyVqTMmSExZRYyrjJzFZMke2bQexW5tTgjTGZ84BSSiQ

FAlVhjLtA2ujyoeU7HdJbt0QZSa7tF/DrqYFtzPw3ixlde7UDt1WH5RiNVZVXYxQ

yQHGx4Kg4IIhNt2UV2Xzp1xcS0x5nbb+bfFTSMLIrUCHL4QRhQ8AGBeYlunEyWGS

qIuWyF2zYA2ctayobSo1YEf+SiM29Uq65xb8bXWfxCmQE3XtELdK2peaDUT4YNCr

Z6W015VRACmqWgnN6Mhcue5lwIs3uVyZdOCdbJqRA9EG1zVIbYbX5NS1emXZp6XX

l3ZtKwUdXdxfzKpbQlWYhWVbhixTgqsgHQI+DZAxQL5mbsBDko3HhozR9VDdeUZf

WweV4UfxrmLcsnEqka9sFVNysBhFED8WFivgw1rLaNn6J/GRuWqlaAPm2nNKJa6Q

RIuFBK5CtsDXjUPl4XU+VgtFNmbYuNqDTC3oN8rWTwYMZ/NKw0gWFizgRNyXcq0Q

AAwFWDtgTuOYSQQcWeLUA9QPXCwEAoPa5kj1SGpk0SAJXXrXBZBtQtVVpb3hG0SA

EPcD3Q98eOtxwtBuiZXy2BbeUhcMviN7WEAl4FMC3cgoMEFu6rqrlFvVhnWM3dl/

2So2TdtfvwppFzML1qcOQSh+R843mNDUtUXChMHUmJ/O6QncDRNsndosNbFXyFCN

ShbBFImSXXKKTPKnDpVJOT43ntVhZe2RdL5ViExd04ednxd8rSAnd62Dd3VAiEmu

Fp+McAEcCMAWIsGUQAYWlJp29DvRGAFd0ZTd5ZNYTjk1ldaPamWzcVXRICu92moW

we9RQAp24FJoIzCSxQmJkJmOdgqW2adVEC0DsQxILZAjAeDvT0oiY3Uz2/FLbWHV

Eta6R20EytIiDUDK52JmifsSzjAXSx6KvEBkMmaL8LBId/H0kEYlIkEDhh7nTolz

t7LYXkJVf/L51QheOVDlJMl1ViW4avjbr0RdDdcg3Rdt7W6X85+AuE0W9kTcmmh9

KTXE0xA4tcU2pNu/XD3kkCPSZq7G/vYvqhtsoeG3sNW/SU1MAMQNH0TFn3apZ44F

FqY7e19AGFyWlPQB0B8WufWxJmxNWfi0ADDWW22qNVDqS3MKTLvTDyp5qfYqLNN/

GBaQ8+6Y0yW0zPHL1/ha5Yr2ke0yslU3NJIhEgMg53dXXa9YXcBk3dRNVq4qWj3S

b2YZCXeb2d1v3Vb2VkOcN67cCe8KyFc1VWeLXWQrA1voviw8JwMVe3A0f2FdvVqE

6ma5/WWnldBkZV0m1JuXwPsDggz0BcDdXaMU6h4xcdVmGBbV2nw8rPN7WjG7YIhB

QAxANOCYgYwIhDWQUwORhsAcbr2BW16US9Ul+BfZQrjNbPZM3NZ0zSHqw0qzoDUj

lUSK/XBqsIMTAEY6LruW4effjw799BeSK7edGYdVhGN37KY5a+OylXmQ8ftNiCi2

pjuEhn2IAp6A7ZSLTA0kDZ7WQP0WevcaX01+bAgCFsxbKWzlslbNWwUAtbPWzAtB

ZaC2UDE4dQPBNuIRdlBR+ZasHixbUMwlVRupI03zF4Mg+7hsMAB0AIALQKyCkAQL

U4OM9oecz2jdbg8o0eDQORukg5WOIY2X22NpO2oWTGatrymigSzyEgcpQmF99rqT

s27dmkgd0o1UIffHSO2KjqUZVIrVlUINq/gb03tNA44VFVbddTWKtlvVE1T0lEbv

BwAikPSGKQMAOoC2E9IfkDYAGgFl1gjQeKgCQj0I7CPatnAAiNQASI361iDeCRIN

n9wbRf0yDi1Rj0396ANODgj6I1COzeWI5G7W1iI8iOP9Wg46IFteIDNJtU4w5lo6

oc4BQBTAEVF8DxS+Dgz359Kw4X0gDkeSX3ttJLcamMIoFKUh1M2SRva0t8tmEiSi

BzAjlMmIclnUKl/9TEPupdw9jnRRjw8XVgRFJKwpsJwXa/FPNnwy80NFVOYyR5sB

bEWwlsCQGWwVsVbDWx1s9pUuJGZXOYaIhg1Ud0PBJz3VTU2ZWDRv0kh6ALuYutBr

R1x1cePSyG0jkI6gBAwlTSiPxjure1yrcKYwyFpjikBmPsEcgASPe9Wxl/5+9pI9

IOB91/SH1xjRALmMrcnXAWM0jaI+mOZjZY2yPdxloDoOdI7loq6YuDJWW21AuAGM

BDA04N2CVwmkfebij6w8N3AD7hiZ2l98o/cVDwISETAiFmfizzSlNHqEZR2/SMji

xIT+ey5XD2dTcNedJo3lSxgeAxQaRg3KG1DfOJ7eYWhdordd311t3VQNG90LbQPu

59A4mkuKFZIhB1czAAwJEcA3soMojIEwCDgT64BwNChogxWNihgbdWOldZI3WPG1

83ByGgTcE5BOChag4OwVN7GNtx4ptTUnDTZvUtA2VlKfRg4bAVtYZbTgl4CMDuF3

kGwABAQgJIA9AyEC7pjOx9YAMdlS41cU8l31Wo031XbX8y0iJHTKxE0i8u7EOd1t

pkLVIJIIKZm2SajwDSgcMupyXjYikkCaTnoChZeYz/DwyZxhzHDQHldHveHiqz7J

bS6B2yGwhCYoQ1P3ceM/QTVHZM6lF0k1P4wPmwtfQ8e7ETVTdtznlZlYDJfB7IHE

glt11an0OaOgPgDIQfQGG4ngB5JaWKQVYGpiwqDPiINijefQuMLpI3efXF9E3VM3

bDL1ocoX4x0PiCRI2rPZ2NAoVQdCswfaO0gdI6k5pO4A2kw40Viek56aGTSOMjiE

gBsBwrXN5o5u2WToWHfQilWueA2xAbseSmY1zk4v7vj5A5+MdDv8d5OxpCfuU2sY

JE2sHSiqlhnFNE6Nt7VTAjQGMBQAxIFRwngrIKcBHYmINgA8AdHHrFjALbpqkXBL

g56q5TK43KNmdkA7+b9IMA/RlIeogYWGQA4kmDkFYKGejay+eo5s35CGk1pMRlA/

TjxdTBkzgNGThYo+wDTJHcfbZhhfPXkeioAk0STTyijZ6NC0EbaN3lb4w6NithNR

K3gtXk0v0dxBPRIABTgZd3FJMvcW57WN3tchDusbjKz6aY+ANgDZAzJI0CkAU6ex

CkcOdS9OvVko64Os9Gwx+ZbD/JTsPpJPzDEjkwOrKgY1TzWlDxP5jaPoXI8vfWIp

wzbUwjNGj+QsjMBAPU8ZMYz24+ZOnNo0/jM2TIMhcrfFiQkgrkzcDa5NfD38YE2L

9fwy3W9DWdisEszJlb2iqWRytwEy03tY5WXgfXZIAJAD1d2DHQ9QGFzMARqD4qWt

2U9KP6eCjWsPyzLAWAMc9XgwqQdUy2RvLw8wspSSXDIM086HJNMjNLhhMSNeX6jO

PCbPtTnnbpNwzKM5r5ozfU6ZODT2M/NS4zVk+NOEzdk5QaUYsTJr3CtpA4tNlDc/

V+OdDa066WMzfkyj6hzI7PrOmGEiL7SFi9JRMO5+MIwgBxAUwErW+sjQEMA8APQJ

pjo40oCyAr0/E6S6cluqYVNFzxU8rO+GNBtLYdstMoSobyXljiBAWrGcjin4JFtF

XtzZs2NlIz3c1bOozvUyZOYz9s8NNIaUk2NMEztk4LJ9tUOcQPeNJQ/POW+/jfP2

St/s2GOytsluvMhzW04FNrBa2qpaEgLCLfk6l7XYyXVAmAKcBCAPAPhl5QIwMSDs

QbAO2ATx2sRQKAeBnVKPLjIk55XfTCo8SI9yCOdVjWeoWJEHUmNLKeXo8BanzhDt

v9aKCQLOk9BqWz1s+jP9Tds0NNF1I02gtOzE0xPM405KfSI41F3R8P41Ps/iWeTZ

yivOfls4ZQscGm87QuY25E8fz1oIFswviN0U+gCKQmmOxCvJJ4D069gMANZCsgQg

EsLtgxqJphxAxAEMBiLcs8Z2SLpndfXmd7iGTJWd8A6bbAET4YEpk87pLJOAE6Cz

IUViuix1Ndz+k3Au9zCC7bNmTpizSk4zjs9ZNWLFyqBTy82rLtl4L0/Tr1uTiDR5

M/DpC/3nrTni8HPeL1C6zOSpspqzAouS8pyhtdIS3ROioxQKcDJApwIhBjAE6XAB

jAxHKyBfA0IjUD4AfQLp5PzEKVksRin0+ANwepc27EWgUUQtqYWczdSbW2KgbXm9

oiHREMztOi61MdzcNfIUigBi/As2zxi+0tDz4xCPPoLzs0TOukNvCIyfk806WFUz

H4+K0TLjdVMtk1Pk0HOklREwssmVekqpYMOHWRkne1MjT/IDA/YPUA6Al4LgoIQR

wckAdAHAGux8TSwxKNADzbTnPuDis/2XiTv5pHJOeZ+qY3UGoNVsoHNNBnEyJxuK

tFU1A2ANlgb887eCuQrLS9CsDzWMxZMWLPS+POkW+sJkOZiGK7JlYrS0zisiWfs/

TMBzd7TC5zLJK24A0LinV+khTN8EJiZSldSwujj0oMixeQByCMCVwJ4FWAjAiEGk

D9gikF2A9As4wN3zjBc3lNCThLUVOeDJU0s6uivg4yzdUf5PJMdU3WjwEQUTIAO2

hj549Br1Lnc/ouwLhi/3NILHS1oXDz3S2POYLMpmzBNU7Hmav3lxNtTPuT1q64vp

SDMxtNMzLgKSuYSyNci1ye/5OiVDjynrROTDZ4l6wnmxQFWABIiEKQADQfnDRFjA

LQCyCZL70wmuPLxc2mulzRa9+QndjaGRhmgZS+6Dxo7SCxr0yu+MhoQLIK1Avbd7

RJquepuiH3OILJi3CvmLeMwavNr943rDy0xtvYvFDIy6UOELevcQt0zbiwOuzLxK

8xg+L4sSvL+LKDi0gYM3tdgDIsEQKQDEAUwMXTwq3YF8DWQAQHACaxzJY/M8ruU2

9OWWb8wDlPLU3bfWEgXVBboIFbovByg1sOqfwwrPPYATQzkQ+0TlrYKyaTvrxeY1

itLMK4PN6r/602suzsVqfbcKQjB2uUzTi46PjLva5Mu2rZC3F2wZQ6yxjOriyxSV

qsBbZosMsphTOtRTWy+gAngPQEMAyNbrNKAkYNQNKC9g2QG16cA7EMQBxE1Gwmu0

b0zmN0HrH8+o2cBr6VjXU8/5HTLUm8uo+H9ZwYM5YWSLU/DN6LMC00vVr367Csyb

o8xgvybQGwqwX8xfEn1V1wyy5OjLzi1e1abcG3asipTaRU4mV+4+6vG6qQKqQTlP

q6EsQAjQHAAIsEmr7YjAwHgMByAOgJXBCAL4C0DPTg3TRuyze69ktfVUi3ksH8MN

tkmmOIkrB0hyMQgKI4Y2MRta/W0aYgO8Ay9mUgkdoTG7ErLSqyqumgaq4jOigYm5

uWNYrDptbkY5+qB2JdnS1CFfkyPK0isw3PBSSZ1sHBR5pA7WbguntEGwQvLRNM7i

sL92m9Murzg614uDsxQAsLqAwBRMVr1YcsHiO8MrLTwYtRwZpi9gQgGFxDAOgN2B

VgikHJDOQlcIODGDu63RuBbOS6uPSL647VMMwDLQF4QaA7fJOysbLLP6xIHSETTv

lpazjzKrqqylvXbVa6R6kY9JtmoS7zMPKKaz3fZXXvDc8xasLzFA7TPE1VWzptPd

RPtxWU5vFRAXh2rRWShOeMu1nzG78lU3yjhbOsMku2jHdWbB0rADoADQWyKMYWIA

MGgCHAT2LihYJuwOMg9jSyyYb+LKrDioXDFPZpi8gCQIhDSgyECMDU7AWx9N07X0

/NsyLjQDHSCSp2BFMtITaNFt3pLLh7z9x7aZt3550C6LtpbqMyA1PDjibzvCNcxQ

80hd9o2pvdrGm8Zl9rXQ9DseLoTXRoMDOXkwOgjV4B4KoAiEIkv4AxAMIDaUiTUf

6Xg/e4PtLAI+2wTljAbb72SDNY8w2xOV/VhPxKHYJPsJAA+0Puz7egvptjFRm8YI

Vl7tWSApa+sJBnJ9Vm3Otrw98vUAngcQJXDHQEQPgCVwVYEMBHkiEJiA/AMe7cu5

z+U0X207s27ktiT+S7TDoMCJebbSxKGbmvVY5oKF4Sqx6cTSF9bRHHyQgL68HEig

e2ggAdIPJvFvfkYWIyJPsOQqoGgN8B/3K+0wBCji5DMkIdDoau+EMvA7pW5Btg7P

a83uVb/a9Vtrzjq+KkItSGm7XUlJoC1id9t697U9AR5IpDZA1kDAA1Ase1yVAH4d

fTtJ7jO7SCHy5KS0YkdKge1SnYMA7Ewx0RjsybaLV3B4oFAIu/kLYHuB6R7+gVeb

9vbIjojKXxWKm/XtXdlq+DuabeK1DsErMyx3ur9bGuv097m/YuyTwRO7l0xtl4Fk

A0CIE+uBO93VakHBHbNT97hHFCAPs+KeGwSPSuMZVWNL76E7WN5Nsg5SMNjt+/Ee

hHS3KgBJHkR6kdR9+mw11LLl/MwkzENWNOtX7I4+1sVZuADwCac0IHXG+b86f5vy

H8e8AdKHoBz9PuIByPdtNEsYJUthq9nXCVOJZItKw7OsvYXuKl5sykgWHzSx+uNo

B5Ud1c4vhDzzUTiu/gvK7UG4vMrTMfu4shNEY4CM2SBYhFgMLxzNXxDDNNUmmxjE

AOEf0AhAA14Xo/XePsVk7x58dGWI0P12Rlv4r8KZHqE9kfI9uTaFmsNBTZj3oA/x

18dAnhE0hsjrtC26vjryWs1q4YVSOp2zrufqQCDgxqKaC710oB6AwyiELyDbrL2L

/J3mcazlN+bU2zTsDHih4nvDHMi7qQSihKk/lm2wRmkI9S5GIn3Q8TIFFVGHKSMJ

vy9om2LtarRizqvILZi6guybOW8isFwmMW5ZgbJWwtPHHrB03tBjhvfBu+TPB2H4

MJMfQqwB7jW63RtY/5HifX7ufq6PVD7o3UPejjQ80NyHr8woeyjjGy1nXh5IN+TY

MlMMEa5KYhYh54gV9r6ntITR86nXDDS9BrrHPJnL4Fi0B1kX0Z2vvKy0g8OplKba

OSc1GxWwNQdC4BhxyDvanPiRVt4r+Epv6/j/wyElD5u+ah3/ltBKQBGDJg2YMWDV

gzYN2DDgzhXtM2Sedj+FepJEHgLIQVbbfkraKSAXM7PA0wCx5ScjrD53ygx3LSTH

UxW86VReTGAFHFcjtcdCyTx3LJ+u6smYYzQCfzHMxFcmedUgkOmeueptuFUegfOI

ZUJZAw6afNaL/eO3m6Vhpss37bU8wDEANQMz6LD2c1ql8rqwwVMenKa0rMhbsiQE

gr2oWJ0xRp0jCgbtQ8Md4gMw+QaekbNgmzFWYDC7Rc4frn4XeNpxxtDyg0yfMp7O

XdXa9ituH7B9owRpO+KjYGnVx8AmATvehWRhunEca0TQ7EVWD0htISiPMXvrmUDs

XnF4jJe9C+4j1oTUJwH15HFI/KHwnEADxdlufFwPscXoWojJ+7FJcjzDDdoD7QPD

lmy0fWbGAF8BVgg4F2DEgpxX/sMBec0Bcsnnp4eufz6a+XNEqwjf4Qw5BfDSL0yx

0IyidI4SBgO51WA4u0frOpRqUymDIPDoo4Th54kN7ZF2wdWBKXMcyaLtF6b2RjmD

R3Xd7Lx1xpTA1kJAmah3F+lctAmV0Jcn9yZaJeT1+tdKEsNFXQUfyDq8GlcZXBE0

fQ6oAQKQDEqg4MQADAJbIQCfy3YPMM8ApAGsUtug7L3A1cgZT0oQXJYoqiTtsNKd

tt+PVGVjo4LSOIzSu+tMTTnNmNSFgdysOkjZims/ufqn2Qyr9girrc8aPTy6qyaR

xnpl6cEh1+czNusnXp7Xt2jYVy4fo1qAO0i9SuQSOxfWBbb4ggWE0cOOHzje6mCq

yUV7qauQk7XFeYZdyGgESAiAKjpckCAJPSsg0oGaCFA0oAgA8Aw9AgBkg+kAkDFA

cQNgCtI0IDgcx00IJpNQR0oCwg+sgMKoDOASGHohYYQsUAotFrHIWzP+zgEBDQAT

oJmmioUHJ4AMAsEhQBEcqx+0TFAgt0LfvAyZSIC60UwAUA6AqwGhep6otxuCgqkt

3zfF75h42g4HGxxWli3Ct8YDFk8azuxy34t5LfS3gkyJT63Wt1LdGdjNKbeY6kt0

ZbAHVt+4WS3HVcVP23Et9rf+HMwi7eS31kPiGPrUSprfW3xgB7jqRhV57fGARt6p

suHod+bfLnG0j2xR3VYGxUdB65yLeRM8twHc6Ag4EHjrg4d84ekXiISQAd6cd9oC

qg/YAQD5Aph1kxTqEUHGv/AVUhVDbgQAA===
```
%%