
HIPPA ~ Protected Health Data (PHI)
- https://www.hhs.gov/sites/default/files/ocr/privacy/hipaa/understanding/coveredentities/De-identification/hhs_deid_guidance.pdf
- https://www.hhs.gov/hipaa/for-professionals/privacy/special-topics/de-identification/index.html

HIPAA and blockchain:
``HIPAA [prohibits the use of mathematically-derived encryption](https://www.gpo.gov/fdsys/pkg/FR-2002-08-14/pdf/FR-2002-08-14.pdf) of protected health information because the encrypted information can potentially be re-identifiable``

-> problems
1. pseudo anonymity
2. access control for psychotherapy notes by patient
3. deterministic encryption

our approach (see also <https://shqb.ml>)
-> use "randomized" identity
-> capability based security (!= ACL)
-> seeded deteministic encryption


### Privacy by design
- everything is encrypted ar REST
### Privacy by default
- we don't ask for information we don't essentially need.

### (un)lawfulness of data processing

Processing shall be lawful only if and to the extent that at least one of the following applies:

1.  1.  the data subject has given consent to the processing of his or her personal data for one or more specific purposes;
    2.  processing is necessary for the performance of a contract to which the data subject is party or in order to take steps at the request of the data subject prior to entering into a contract;
    3.  processing is necessary for compliance with a legal obligation to which the controller is subject;
    4.  processing is necessary in order to protect the vital interests of the data subject or of another natural person;
    5.  processing is necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller;
    6.  processing is necessary for the purposes of the legitimate interests pursued by the controller or by a third party, except where such interests are overridden by the interests or fundamental rights and freedoms of the data subject which require protection of personal data, in particular where the data subject is a child.

Point (6) of the first subparagraph shall not apply to processing carried out by public authorities in the performance of their tasks.

### Data privacy questionnaire:
What data you are collecting?
Why?
How many people have access to it?
How many people actually need to have access to it?
Should it be permanent access?


Checklist
- [ ] maintaining data transactions log/ledger/registry
- [ ] bio metrics and genetic data are classified as sensitive data.
- [ ] [[Safe Harbor]] method (18 identifiers)
- [ ] Satisfying Expert Determination Method
- [ ] very small identification risk 
- [ ] conduct an information audit to determine what information you process and who has access to it.
- [ ] have a legal justification for your data processing activities
- [ ] provide a clear information about your data processing and legal justification in your privacy policy.
- [ ] take data protection into account at all times, from the moment you begin developing a product to each time you process data.
- [ ] encrypt, pseudonymize, or anonymize personal data whenever possible
- [ ] create an internal security policy for your team members, and build awareness about data protection.
- [ ] know when to conduct a data protection impact assessment, and have a process in place to carry it out.
- [ ] have a process in place to notify the authorites and your data subject in the event of a data breach.
- [ ] designate someone responsible for ensuring GDPR compliance across your organization.
- [ ] sign a data processing agreement between your organization and any third parties that process presonal data on your behalf.
- [ ] if your organization is outside the EU, appoint a representative within one of the EU member states.
- [ ] Appoint a data protection officier (if necessary)
- [ ] It's easy for your customers to correct or update inaccurate or imcomplete information.
- [ ] it's easy for your customers to request to have their personal data deleted
- [ ] it's easy for your customers to ask you to stop processing their data
- [ ] if you make decisions about people base on automated process, you have a procedure to protect their rights.







