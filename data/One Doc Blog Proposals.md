1. What is menopause? How do you know if you are in it? - Colleen

2. Key Nutrients for your Peri and Post Menopause Journey - Farah

3. **What is Hormone Replacement Therapy? What are the nutrition implications in menopause?**

**4. Is menopause affecting your sleep?** Three Bed Time Routines to make it better.
	*I added a menopause focus in title?* *It really depends on how they position the articles on the site*.

5. Menopause Supplements to Support your Diet

6. **Alcohol, menopause symptoms and solutions**

Its Not All Menopause: Your Energy May Be Plummeting For Other Reasons *(feel like this diminishes menopause. Maybe wrong message for this population?)*
	**7. How about "Menopause fatigue. Where does it come from?**"
		*Can also discuss other sources of fatigue (including general midlife challenges and how menopause augments all of this...ie threshold is less etc etc.)*

8. **Midlife, menopause and weight gain**

9. **Menopause, longevity and nutrition status. What's the connection?**

10. Menopause Symptoms No One Talks About **& Natural Remedies**
	*Excellent. What do you envision here? UTIs? Hair on the chin? Thinning hair? Angry to the point of no return? haha*

11. How to Advocate for Your Health **in midlife, menopause and beyond**
	*Note: Ät any age may not be focused enough for one doc midlife/menopause section.*

12. Will I lose my sex drive in menopause? What can I do to prevent this?
13. Menopause and your microbiome!
15. Menopause and mood. What's the hormone and nutrition connection?
16. Brain fog. What did you say? Why me?
17. Stress, anxiety and panic in the pause. Herbal remedies to the rescue.
18. Menopause and the gut x brain connection.
19. Healthy skin aging: inside and out.
20. Menopause at work. You are in the change, so be the change!
21. Diet and supplement solutions for heart palpitations.





--------------------------------------------------
Save for Later:

Menopause for the Generation that Lived the 90’s to Early 2000’s Toxic Diet Culture (this might be more relevant to the US group)
	*I love it.** **Let's save this for a longer, provocative PhenomX blog post.*

Menopause: sex, love and sleep!