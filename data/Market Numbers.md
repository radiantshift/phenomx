
![[Recording 20221011212301.webm]]
## PhenomX Market Share Analysis
(Numbers Assumptions made between 2/27/2022 - 3/11/2022)

2.6B = 3.847B * 68.41% women 15-70y/o

67% with symptoms (see Maria's xls)
85% with mobile
38% using top health app (fitbit)

250M women age 40 to 54
44M woman in (us,ca,eu...)

![market analysis](market-analysis-w-numbers.svg)

Source Statistica & Worldbank

women health market 15B ([menopause](https://www.grandviewresearch.com/industry-analysis/menopause-market) market by 2022; on [ipfs](https://hardbin.com/ipfs/Qmeg4sT3qVvVWDM82x5FRJCkZbkzb7y7sFKr95TbSS5Vc6/))
w age 15-64: https://data.worldbank.org/indicator/SP.POP.1564.FE.ZS





