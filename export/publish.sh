
find . -name '*~1' -delete
tic=$(date +%s%N | cut -c-13)
qm=$(ipfs add -r . -Q)
echo $tic: $qm | tee -a qm.log
connect.sh
curl -I -s https://ipfs.safewatch.care/ipfs/$qm | grep -e ipfs -e Etag
echo url: https://ipfs.safewatch.care/ipfs/$qm
