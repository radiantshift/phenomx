# Symptoms classification

## categories names (label)
(theses are the main categories)
 - physical
 - mood
 - aesthetical
 

---
## Coloring Method (tags)
*note: the first tag is the label (== unique category); multiple tags per symptoms*
- hotflash: physical, limiting
- wrickles: aesthetical, skin


```yaml format
## categories names (label)

Physiologic symptoms:

  Brain health:
  - Mood swings
  - Depression Headaches
  - Brain fog Anxiety and/or panic attacks
  - Decreased memory

  Gastrointestinal health:
  - Bloating
  - Gas
  - Constipation
  - Diarrhea during menstruation
  - Dry mouth

  Menstrual health:
  - Sore breasts
  - Menstrual crampss
  - Vaginal dryness

  Climacteric:
  - Hot flashes
  - Night sweats
  - Cold flashes
  - Body odor
  - Heart palpitations

  Sleep issues:
  - Sleep difficulties

  Metabolic health:
  - Weight gain

  Heart health:
  - Heart palpitations

  Bone health:
  - Joint pain

  Immune health:
  - Frequent sickness
  - colds
  - Urinary tract infections

  Energy:
  - Low energy
  - fatigue

  Bladder health:
  - Urinary tract infections
  - Bladder pain
  - frequent urination
  - Incontinence

Aesthetic symptoms:

  Skin health:
  - Loss of skin elasticity
  - Skin sagging…Skin wrinkling
  - Age spots/melasma
  - Dry skin
  - Hair loss
  - thinning
  - change
  - Skin rash
  - atopic dermatitis
  - eczema

```

![[symptoms.yml]]